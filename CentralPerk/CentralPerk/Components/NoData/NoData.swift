//
//  NoData.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 21/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

enum NoDataType {
    case noInternet
    case unknown
    case noFriends
    case custom(title: String, description: String)
}

class NoData: UIView {

    @IBOutlet weak var imageCenterView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialize()
    }
    
    private func initialize() {
        
    }
    
    func configure(type: NoDataType) {
        //TODO:
        self.labelTitle.text = AppException.noFriends
        self.labelDescription.text = AppException.noFriendsDescription
    }

}
