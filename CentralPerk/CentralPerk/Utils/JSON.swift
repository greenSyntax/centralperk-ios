//
//  JSON.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 21/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation

class JSONParser {
    /// Get Codable Type of Object when you pass the Data
    ///
    /// - Parameter data: Data what you get on Network Response
    /// - Returns: Generic Type<T> i.e. a codable type
    func get<T:Codable>(withData data:Data) -> T? {
        return try? JSONDecoder().decode(T.self, from: data)
    }
    
    func set<T:Codable>(withObject object:T) -> Data? {
        return try? JSONEncoder().encode(object)
    }
}
