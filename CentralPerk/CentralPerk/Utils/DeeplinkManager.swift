//
//  DeeplinkManager.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 21/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation



enum DeeplinkType: String {
    case invitation = "invitation"
}

class DeeplinkManager {
    
    func parseURL(_ url: URL) {
        if let components = url.queryParameters, let type = components["type"] {
            handleType(type: type, payload: components)
        }
    }
    
    func handleType(type: String, payload: [String: String]) {
        switch DeeplinkType(rawValue: type.lowercased()) {
        case .invitation:
            if let uid = payload["uid"] as? String {
                addUser(uid: uid)
            }
        default:
            print("Unhandled Deeplink")
        }
    }
    
}


extension DeeplinkManager {
    
    private func addUser(uid: String) {
        if let vc = Utility.getTopViewController() as? BaseViewController {
            vc.addYourFriend(friendUID: uid)
        }
    }
    
}
