//
//  AppText.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 20/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation

struct AppText {
    
    static let appName = "Indiee :)"
    static let appNameFormatted = "Indiee"
    static let appDescription = "Say Hi, to Your Friends"
    static let author = "Green Syntax & Team"
    
    static let successfullySignUp = "Wow! You've Succeffully Registered."
    static let successfullyLogin = "Hey, You've successfully logged in."
    static let successfullResetPassword = "We've sent you a reset password link. Please check your email"
    static let successfullyProfile = "Great!, You've Successfuly Created Profile"
    
    static let onboardingButtonText = "Let's Onboard Yourself"
    
    static let loginHeadText = "For Existing User"
    static let signUpText = "For New User"
    
    static let inviteTitle = "Share With Your Friends"
    static let inviteDescription = "Share with your friend and ask them to download app & join via link"
    static let inviteShareLink = "Send Invitation"
    
    static let chatTextFieldPlaceholder = "Type Your Message here ..."
    static let landingFooterText = "Yes, I'm accepting the terms&conditions and Privacy Policy."
    static let privacyPolicy = "Privacy Policy"
}

struct AppException {
    static let noInternet = "Bro, You've No Internet."
    static let noInternetDescription = "Ah! You lost your internet connectivity."
    
    static let noFriends = "You've No Friends."
    static let noFriendsDescription = "Add your friends by sharing Invitation Link"
    
    static let emailRequired = "Email Address is required"
    static let mobileRequired = "Mobile Number is required"
    static let nameRequired = "Name is required"
    static let passwordRequired = "Password is required"
    
    
}
