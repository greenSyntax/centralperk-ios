//
//  AppError.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 16/03/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation

enum AppError {
    
    case authenticationFailed
    case apiFailure
    case serverError
    case documentNotFound
    case illFormattedData
    case custom(messge: String)
    
    var text: String {
        switch self {
        case .apiFailure:
            return "API Failure. Please check logs"
        case .authenticationFailed:
            return "Authentication Failed"
        case .documentNotFound:
            return "Document Not Found"
        case .illFormattedData:
            return "Something went wrong with stored data."
        case .serverError:
            return "Server Error. Please try again"
        case .custom(messge: let data):
            return data
        }
    }
}
