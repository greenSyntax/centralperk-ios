//
//  Utility.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 21/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation
import UIKit

class Utility {
    
//    class func getTopViewController() -> UIViewController? {
//        if let top = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
//            return top.viewControllers.last
//        }
//        return UIApplication.shared.keyWindow?.rootViewController
//    }
    
    class func generateInvitationURL(uid: String) -> String {
        return "sandesh://deeplink?type=invitation&uid=\(uid)"
    }
    
    class func getTimestamp() -> String {
        return String(describing: NSDate().timeIntervalSince1970)
    }
    
    class func getDateFromTimestamp(timeStamp: Double) -> Date {
        return Date(timeIntervalSince1970: timeStamp)
    }
    
    class func showAlert(title: String?, description: String?, positiveText: String, negativeText: String, onPositiveAction: @escaping () -> Void, onNegativeAction: @escaping () -> Void) {
        let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: positiveText, style: .default, handler: { (action) in
            onPositiveAction()
        }))
        alert.addAction(UIAlertAction(title: negativeText, style: .cancel, handler: { (action) in
            onNegativeAction()
        }))
        self.getTopViewController()?.present(alert, animated: true, completion: nil)
    }
    
    class func showActionSheet(title: String?, description: String?, options: [(text: String, handler: () -> Void)]) {
        let sheet = UIAlertController(title: title, message: description, preferredStyle: .actionSheet)
        options.forEach { (tupple:(text: String, handler: () -> Void)) in
            sheet.addAction(UIAlertAction(title: tupple.text, style: .default, handler: { (action) in
                tupple.handler()
            }))
        }
        sheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            // TODO:
        }))
        self.getTopViewController()?.present(sheet, animated: true, completion: nil)
    }
    
    class func getTopViewController() -> UIViewController? {
        if let rootVC = UIApplication.shared.keyWindow?.rootViewController {
            if let contrainer = rootVC as? ContainerViewController, let centerVC = contrainer.centerController, let navigationController = centerVC as? UINavigationController, let topVC = navigationController.viewControllers.last {
                return topVC
            }
            return rootVC
        }
        return nil
    }
    
    class func semantic() -> (version: String, build: String) {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        return (version: version, build: build)
    }
}
