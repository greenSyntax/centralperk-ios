//
//  UIImage.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 13/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    func toBase64() -> String? {
        guard let imageData = self.inJPEG() else { return nil }
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    func inJPEG() -> Data? {
        return self.jpegData(compressionQuality: 0.5)
    }
}
