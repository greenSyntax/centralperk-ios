//
//  Date.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 21/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation

extension Date {
    
    func getFormattedDate() -> String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "d MMM, h:mm a"
        return dateFormat.string(from: self)
    }
    
    func getFormattedDay() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM yy"
        return dateFormatter.string(from: self)
    }
    
    func getFormattedForMessages() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter.string(from: self)
    }
    
}
