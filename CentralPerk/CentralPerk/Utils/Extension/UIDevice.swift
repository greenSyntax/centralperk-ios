//
//  UIDevice.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 14/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

extension UIDevice {
    class var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}
