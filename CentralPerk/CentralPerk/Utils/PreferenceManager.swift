//
//  PreferenceManager.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 21/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation

enum KeyName: String {
    case userId
    case isPushNotification
}

class PreferenceManager {
    
    private let defaults = UserDefaults.standard
    
    static let shared = PreferenceManager()
    private init(){ }
    
    /// Get Value for the Key Name
    ///
    /// - Parameter key: Name of the Name which is of type KeyName
    /// - Returns: Value of T type
    fileprivate func getObject<T>(key:String) -> T? {
        if let value = defaults.value(forKey: key), let castedValue = value as? T {
            return castedValue
        }
        return nil
    }
    
    
    /// State Object to UserDeafults Key-Value pair against their Key Name
    ///
    /// - Parameters:
    ///   - key: Key Name as String
    ///   - value: Value for the Key
    fileprivate func setObject<T>(key:String, value:T) {
        defaults.set(value, forKey: key)
    }
    
    
}

extension PreferenceManager {
    
    // UserID
    var userId: String? {
        get {
            return self.getObject(key: KeyName.userId.rawValue)
        }
        set {
            setObject(key: KeyName.userId.rawValue, value: newValue)
        }
    }
    
    var isPushNotification: Bool {
        get {
            return self.getObject(key: KeyName.isPushNotification.rawValue) ?? true
        }
        set {
            setObject(key: KeyName.isPushNotification.rawValue, value: newValue)
        }
    }
    
    // Clear
    func clear() {
        if let bundleID = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: bundleID)
        }
    }
}
