//
//  RemoteImage.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 20/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class RemoteImage {
    
    private static let PLACEHOLDER_IMAGE = UIImage(named: "img_placeholder")
    
    public static func get(_ url: URL, imageView: UIImageView, onCompletion: @escaping (_ isSuccess: Bool)->()) {
        imageView.sd_setImage(with: url, placeholderImage: PLACEHOLDER_IMAGE)
        imageView.sd_setImage(with: url) { (image, error, type, url) in
            onCompletion(image != nil)
        }
    }
    
    public static func get(_ path: String, imageView: UIImageView, onCompletion: @escaping (_ isSuccess: Bool)->()) {
        guard let imagePath = URL(string: path) else { return }
        get(imagePath, imageView: imageView, onCompletion: onCompletion)
    }
}
