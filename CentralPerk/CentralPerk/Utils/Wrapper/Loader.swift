//
//  Loader.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 20/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation
import NVActivityIndicatorView

class Loader {
    
    private var loader:NVActivityIndicatorView!
    private let size: CGFloat = 20.0
    private var wrapperView: UIView?
    
    public func show(view: UIView) {
        self.wrapperView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        loader = NVActivityIndicatorView(frame: CGRect(x: view.center.x - (size / 2.0) , y: view.center.y - (size / 2.0), width: size, height: size), type: .circleStrokeSpin, color: .black, padding: nil)
        loader.startAnimating()
        wrapperView?.backgroundColor = UIColor.appLightGray
        wrapperView?.layer.opacity = 0.7
        wrapperView?.addSubview(loader)
        if let wrapper = wrapperView { view.addSubview(wrapper) }
    }
    
    public func hide() {
        wrapperView?.removeFromSuperview()
        loader.stopAnimating()
    }
    
    public func addLoader(_ view: UIView) {
        loader = NVActivityIndicatorView(frame: CGRect(x: view.center.x - (16 / 2.0) , y: view.center.y - (16 / 2.0), width: 16, height: 16), type: .circleStrokeSpin, color: .darkGray, padding: nil)
        loader.startAnimating()
        view.addSubview(loader)
    }
    
}
