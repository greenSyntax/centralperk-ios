//
//  Toast.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 20/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation
import Toaster

class Toaster {
    
    class func show(_ message: String) {
        Toast(text: message).show()
    }
}
