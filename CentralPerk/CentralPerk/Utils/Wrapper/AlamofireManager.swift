//
//  AlamofireManager.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 26/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation
import Alamofire

class AlamofireManager {
    
    func isInternetAvailable() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
    
}
