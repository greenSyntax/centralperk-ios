//
//  ImagePickerManager.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 21/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation
import UIKit
import Photos

protocol ImagePickerWrapperDelegate: class {
    func didSelectedImage(_ image: UIImage?)
}

class ImagePickerWrapper: NSObject {
    
    weak var controller: UIViewController?
    weak var delegate: ImagePickerWrapperDelegate?
    
    func checkPermission(onCompletion: @escaping(_ isSuccess: Bool) -> Void) {
        
        PHPhotoLibrary.requestAuthorization { (status) in
            switch status {
            case .authorized:
                onCompletion(true)
            case .denied:
                onCompletion(false)
            case .notDetermined:
                onCompletion(false)
            case .restricted:
                onCompletion(false)
            default:
                print("NA")
            }
        }
    }
    
    func show(_ vc: UIViewController, delegate: ImagePickerWrapperDelegate) {
        
        checkPermission { (isSuccess) in
            if isSuccess {
                DispatchQueue.main.async {
                    self.delegate = delegate
                    self.controller = vc
                    let pickerController = UIImagePickerController()
                    pickerController.delegate = self
                    
                    pickerController.mediaTypes = ["public.image"]
                    pickerController.sourceType = .photoLibrary
                    
                    vc.present(pickerController, animated: true, completion: nil)

                }
            } else {
                print("Permission is not there")
            }
            
        }
    }
    
}

extension ImagePickerWrapper: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.controller?.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            self.delegate?.didSelectedImage(nil)
            return
        }
        self.delegate?.didSelectedImage(image)
    }
    
    @objc func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.controller?.dismiss(animated: true, completion: nil)
        self.delegate?.didSelectedImage(nil)
    }
}
