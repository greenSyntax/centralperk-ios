//
//  FirebaseMessagingManager.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 13/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation
import UserNotifications
import FirebaseMessaging
import Alamofire

class FirebaseMessagingManager: NSObject {
    
    /// Regiseter for FCM Service
    func register() {
        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (isGranted, error) in
            if isGranted { Messaging.messaging().delegate = self }
            
        }
        
        UIApplication.shared.registerForRemoteNotifications()
        self.updateFirebaseMessengingToken()
    }
    
    func subscribe(topic: TopicType) {
        Messaging.messaging().subscribe(toTopic: topic.getTopicAlias()) { (error) in
            if let err = error {
                print("Error: \(err.localizedDescription)")
                return
            }
            print("Successfully Subscribed to topic \(topic.getTopicAlias())")
        }
    }
    
    func unsubscribe(topic: TopicType) {
        Messaging.messaging().unsubscribe(fromTopic: topic.getTopicAlias()) { (error) in
            if let err = error {
                print("Error: \(err.localizedDescription)")
                return
            }
            print("Successfully Unregistered to topic \(topic.getTopicAlias())")
        }
    }
    
    func publish(topic: TopicType, title: String, text: String, success: @escaping () -> Void, failure: @escaping() -> Void) {
        guard let fcmURL = URL(string: AppConfig.fcmSendAPI) else {
            failure()
            return
        }
        let headers = ["Content-Type": "application/json", "Authorization": AppConfig.fcmKey]
        let body = NotificationPayload(text: text, type: topic, title: title).toDict()
        
        Alamofire.request(fcmURL, method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers).response { (response) in
            print("FCM Repsonse: \(String(describing: String(data: response.data!, encoding: .utf8)))")
            if let statusCode =  response.response?.statusCode, statusCode == 200 {
                success()
            } else {
                failure()
            }
        }
    }
    
    func updateFirebaseMessengingToken() {
        if let token = Messaging.messaging().fcmToken {
            print("FCM Token: \(token)")
            
            //TODO: Update Token
            //Engine.shared.userManager.updateFirebaseMessengingToken(token: token)
        }
    }
    
    func processPayload(dict: [AnyHashable: Any]) {
        if let deeplinkString = dict["deeplink"] as? String, let deeplinkURL = URL(string: deeplinkString) {
            //DeeplinkHanlder().parse(url: deeplinkURL)
            //TODO:
            return
        }
    }
    
}

extension FirebaseMessagingManager: MessagingDelegate, UNUserNotificationCenterDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        updateFirebaseMessengingToken()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        processPayload(dict: response.notification.request.content.userInfo)
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        if !(Utility.getTopViewController() is ChatViewController) {
            completionHandler([.alert])
        }
    }
}
