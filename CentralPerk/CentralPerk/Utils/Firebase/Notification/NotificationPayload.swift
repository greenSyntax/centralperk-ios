//
//  NotificationPayload.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 13/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation

struct NotificationPayload {
    
    struct MessagePayload {
        var title: String
        var body: String
        var deeplink: String
        var data: String
        
        func toDict() -> [String: String] {
            var dict = [String: String]()
            dict["title"] = title
            dict["body"] = body
            dict["deeplink"] = deeplink
            dict["data"] = data
            return dict
        }
    }
    
    var notification: MessagePayload
    var to: String
    
    init(text: String, type: TopicType, title: String?) {
        to = "/topics/\(type.getTopicAlias())"
        let title = "Message from \(title ?? AppConfig.appName)"
        notification = MessagePayload(title: title, body: text, deeplink: "", data: "") //TODO:
    }
    
    func toDict() -> [String: Any] {
        var dict = [String: Any]()
        dict["notification"] = notification.toDict()
        dict["to"] = to
        return dict
    }
    
}

