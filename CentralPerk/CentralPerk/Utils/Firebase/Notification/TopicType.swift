//
//  TopicType.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 13/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation

enum TopicType {
    case chat(conversationId: String)
    case webLink(url: URL)
    
    func getTopicAlias() -> String {
        switch self {
        case .chat(let chatId):
            return "chat-\(chatId)"
        case .webLink(let url):
            return "\(url)"
        }
    }
}
