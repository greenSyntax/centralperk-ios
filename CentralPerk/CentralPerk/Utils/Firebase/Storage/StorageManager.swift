//
//  StorageManager.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 20/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation
import UIKit
import FirebaseStorage

class StorageManager {
    
    private let storage = Storage.storage()
    private lazy var storageRef = storage.reference()
    private lazy var chatImagesRef = storageRef.child("profile_pictures")
    
    func upload(image: UIImage, onCompletion: @escaping(_ path: URL?, _ error: Error?) -> Void) {
        let uploadImage = chatImagesRef.child("\(Utility.getTimestamp()).jpg")
        guard let data = image.inJPEG() else {
            print("No Image")
            return
        }
        let uploadTask = uploadImage.putData(data, metadata: nil) { (metadata, error) in
            if let error = error {
                onCompletion(nil, error)
                return
            }
            //print("Meta: \(metadata)")
            uploadImage.downloadURL(completion: { (url, error) in
                if let error = error {
                    onCompletion(nil, error)
                    return
                }
                onCompletion(url, nil)
            })
        }
    }
    
}
