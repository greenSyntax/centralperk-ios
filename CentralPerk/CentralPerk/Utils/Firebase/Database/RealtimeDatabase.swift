//
//  RealtimeDatabase.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 20/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation
import FirebaseDatabase

class RealtimeDatabaseManager {
    
    public static let shared = RealtimeDatabaseManager()
    private let database = Database.database().reference()
    
    func getChildId() -> String? {
        return database.childByAutoId().key
    }
    
    func write(child: String, data: Any, onCompletion: @escaping(_ promise: Promise<Bool>) -> Void) {
        database.child(child).setValue(data) { (error, documenet) in
            if let error = error {
                onCompletion(Promise.failure(.custom(messge: error.localizedDescription)))
                return
            }
            onCompletion(Promise.success(true))
        }
    }
    
    func read<T>(path: String, onCompletion: @escaping(_ promise: Promise<T>) -> Void) {
        database.child(path).observeSingleEvent(of: .value) { (snapshot) in
            if let data = snapshot.value as? T {
                onCompletion(Promise.success(data))
            } else {
                onCompletion(Promise.failure(.documentNotFound))
            }
        }
    }
    
    func readWithListner<T>(path: String, onCompletion: @escaping(_ promise: Promise<T>) -> Void) {
        database.child(path).observe(.value) { (snapshot) in
            if let data = snapshot.value as? T {
                onCompletion(Promise.success(data))
            } else {
                onCompletion(Promise.failure(.documentNotFound))
            }
        }
    }
    
    func update(child: String, data: [String: Any], onCompletion: @escaping(_ promise: Promise<Bool>) -> Void) {
        database.child(child).updateChildValues(data) { (error, database) in
            if let error = error {
                onCompletion(Promise.failure(.custom(messge: error.localizedDescription)))
                return
            }
            onCompletion(Promise.success(true))
        }
    }
    
    func delete(child: String, onCompletion: @escaping (_ promise: Promise<Bool>) -> Void) {
        database.child(child).removeValue { (error, refrence) in
            if let err = error {
                print("Database Document Delete: \(err.localizedDescription)")
                onCompletion(Promise.failure(.custom(messge: err.localizedDescription)))
            }
            onCompletion(Promise.success(true))
        }
    }
}
