//
//  AuthUserModel.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 16/03/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation

struct LoginUserModel {
    var email: String
    var password: String
}
