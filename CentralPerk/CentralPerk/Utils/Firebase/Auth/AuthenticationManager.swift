//
//  Authentication.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 16/03/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth

class AuthenticationManager {
    
    func loggedInUser() -> LoggedInUser? {
        if let user = Auth.auth().currentUser {
            return LoggedInUser(user: user)
        }
        return nil
    }
    
    func signin(userModel: LoginUserModel, onCompletion: @escaping (_ data: Promise<LoggedInUser>) -> ()) {
        Auth.auth().signIn(withEmail: userModel.email, password: userModel.password) { (result, error) in
            if let user = result?.user {
                onCompletion(Promise.success(LoggedInUser(user: user)))
            } else {
                print("Firebase Error: \(error?.localizedDescription)")
                onCompletion(Promise.failure(.custom(messge: error?.localizedDescription ?? "")))
            }
        }
    }
    
    func signup(userModel: LoginUserModel, onCompletion: @escaping(_ data: Promise<LoggedInUser>) -> ()) {
        Auth.auth().createUser(withEmail: userModel.email, password: userModel.password) { (result, error) in
            if let user = result?.user {
                onCompletion(Promise.success(LoggedInUser(user: user)))
            } else {
                print("Firebase Error: \(error?.localizedDescription)")
                onCompletion(Promise.failure(.custom(messge: error?.localizedDescription ?? "Error")))
            }
        }
    }
    
    func resetPassword(email: String, onCompletion: @escaping(_ data: Promise<String>) -> ()) {
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            if let error = error {
                onCompletion(Promise.failure(.custom(messge: error.localizedDescription ?? "Reset Password Error")))
            } else {
                onCompletion(Promise.success(email))
            }
        }
    }
    
    
    func signout(onCompletion: () -> ()) {
        if let _ = try? Auth.auth().signOut() {
            onCompletion()
        }
    }
    
}
