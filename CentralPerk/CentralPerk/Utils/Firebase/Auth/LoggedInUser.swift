//
//  AuthUserResponse.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 16/03/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation
import Firebase

struct LoggedInUser {
    
    var userId: String
    var name: String?
    var email: String?
    var phoneNumber: String?
    var profilePictureURL: URL?
    
    init(user: User) {
        self.userId = user.uid
        self.name = user.displayName
        self.email = user.email
        self.phoneNumber = user.phoneNumber
        self.profilePictureURL = user.photoURL
    }
    
    func toDict() -> [String: Any] {
        var dict = [String: Any]()
        dict["uid"] = self.userId
        dict["name"] = self.name
        dict["email"] = self.email
        dict["mobile"] = self.phoneNumber
        dict["profilePictureURL"] = self.profilePictureURL?.absoluteString
        
        return dict
    }
}
