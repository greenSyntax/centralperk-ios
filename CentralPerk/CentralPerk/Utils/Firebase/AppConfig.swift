//
//  AppConfig.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 13/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation

struct AppConfig {
    static let fcmKey = "key=AAAA4IVuYz8:APA91bEyBU4aI3rUAH4NwCXqSa4F0R3SDYjJvybwjRlV3WCvQZ5CXM2glSUiq_A4hckm-kmaTa0VdXvhqFndebLqxqQK3d77MlmnVpuFOkbvL7UCrZYDBDih5ZAvVP917BTd1LmpbWa5"
    static let fcmSendAPI = "https://fcm.googleapis.com/fcm/send"
    static let privacyURL = "https://greensyntax.co.in/topstory/indiee_privacy_policy.html"
    static let tnc = "https://greensyntax.co.in/topstory/indiee_tnc.html"
    
    static let appName = "Sandesh"
}
