//
//  ShareIntentManager.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 21/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation
import UIKit

class ShareIntentManager {
    
    class func shareViaActivity(_ path: URL) {
        let activityViewController = UIActivityViewController(activityItems: [path], applicationActivities: nil)
        activityViewController.excludedActivityTypes = [.openInIBooks, .assignToContact]
        Utility.getTopViewController()?.present(activityViewController, animated: true, completion: nil)
    }
    
}
