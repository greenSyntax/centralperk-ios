//
//  BaseViewController.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 16/03/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

enum ViewState {
    case loading
    case error(error: AppError)
    case done
    case area(view: UIView)
}

class BaseViewController: UIViewController {
    
    private let loader = Loader()
    
    var state: ViewState = .done {
        didSet {
            self.determineState()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.registerKeyboardGesture()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func registerKeyboardGesture() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func registerOutsideTap() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissResponder))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func dismissResponder() {
        self.view.endEditing(true)
    }
   
    func setBackButton(_ button: UIButton) {
        button.tintColor = UIColor.darkGray
        button.titleLabel?.font = UIFont.fontAwesome(ofSize: 20, style: .solid)
        button.setTitle(String.fontAwesomeIcon(name: .arrowLeft), for: .normal)
    }
    
    func setCloseButton(_ button: UIButton, color: UIColor = UIColor.darkGray) {
        button.tintColor = color
        button.titleLabel?.font = UIFont.fontAwesome(ofSize: 20, style: .solid)
        button.setTitle(String.fontAwesomeIcon(name: .times), for: .normal)
    }
    
    func moreButton(_ button: UIButton, color: UIColor = UIColor.darkGray) {
        button.tintColor = color
        button.titleLabel?.font = UIFont.fontAwesome(ofSize: 20, style: .solid)
        button.setTitle(String.fontAwesomeIcon(name: .ellipsisV), for: .normal)
    }
    
    func showToast(message: String) {
        Toaster.show(message)
    }
    
    func refreshView() {}
    
    private func determineState() {
        switch state {
        case .loading:
            self.dismissResponder()
            loader.show(view: self.view)
        case .done:
            loader.hide()
        case .error(let error):
            Toaster.show(error.text)
        case .area(let view):
            loader.show(view: self.view)
        }
    }
    
    func addYourFriend(friendUID: String) {
        guard let uid = PreferenceManager.shared.userId else { return }
        guard uid != friendUID else {
            self.showToast(message: "Bro, You complete youself :)")
            return
        }
        
        guard let friendMapperId = RealtimeDatabaseManager().getChildId() else {
            self.showToast(message: "Sorry. Can't creat friendId")
            return
        }
        
        if let chatId = RealtimeDatabaseManager().getChildId() {
            RealtimeDatabaseManager.shared.write(child: "friends/\(uid)/\(friendMapperId)", data: ["chatId": chatId, "friendId": friendUID ]) { (promise: Promise<Bool>) in
                switch promise {
                case .failure(let error):
                    self.showToast(message: error.text)
                case .success(_):
                    self.mapWithYourFriend(chatId: chatId, friendUID: friendUID, friendMapperId: friendMapperId, userUID: uid)
                }
            }
        }
    }
    
    func mapWithYourFriend(chatId: String, friendUID: String, friendMapperId: String, userUID: String) {
        RealtimeDatabaseManager.shared.write(child: "friends/\(friendUID)/\(friendMapperId)", data: ["chatId": chatId, "friendId": userUID ]) { (promise: Promise<Bool>) in
            switch promise {
            case .failure(let error):
                self.showToast(message: error.text)
            case .success(_):
                self.showToast(message: "Successfully Added.")
                self.refreshView()
            }
        }
    }
    
    func showNoData(type: NoDataType, containerView: UIView) {
        let noDataView = UINib(nibName: "NoData", bundle: nil).instantiate(withOwner: self, options: [:]).first as! NoData
        noDataView.translatesAutoresizingMaskIntoConstraints = false
        noDataView.configure(type: type)
        containerView.addSubview(noDataView)
        
        NSLayoutConstraint.activate([
            noDataView.heightAnchor.constraint(equalToConstant: 200),
            noDataView.widthAnchor.constraint(equalToConstant: 200),
            noDataView.centerXAnchor.constraint(equalToSystemSpacingAfter: containerView.centerXAnchor, multiplier: 1.0),
            noDataView.centerYAnchor.constraint(equalToSystemSpacingBelow: containerView.centerYAnchor, multiplier: 1.0 )
        ])
    }
}

extension BaseViewController {
    
    @objc func keyboardWillShow(_ notification:Notification) {

        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            onKeyboardShown(keyboardSize)
        }
    }
    @objc func keyboardWillHide(_ notification:Notification) {

        if let _ = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            //tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
            onKeyboardHide()
        }
    }
    
    @objc func onKeyboardShown(_ keyboardSize: CGRect) {}
    
    @objc func onKeyboardHide() {}
    
}
