//
//  Router.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 21/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation
import UIKit

enum RouteMode {
    case push
    case present
    case pop
    case dismiss
}

class Router {
    
    static var navigationController: UINavigationController!
    
    class func route(mode: RouteMode, vc: UIViewController) {
        
        switch mode {
        case .present:
            //TODO:
            break
            
        case .push:
            break
            
        case .pop:
            break
            
        case .dismiss:
            break
        }
    }
    
}
