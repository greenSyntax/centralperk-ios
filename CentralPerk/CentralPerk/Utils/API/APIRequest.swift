//
//  APIRequest.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 21/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation

enum HttpVerb: String {
    case GET
    case POST
    case PUT
    case DELETE
}

struct APIRequest {
    var url: URL
    var httpVerb: HttpVerb
    var headers: [String: String]
    var body: Data?
}
