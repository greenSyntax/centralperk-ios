//
//  APIManager.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 21/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation
import UIKit

class APIManager {
    
    private let session = URLSession.shared
    private let timeout = 30.0
    
    public static let shared = APIManager()
    private init() {}
    
    private func prepareSessiomRequest(_ request: APIRequest) -> URLRequest {
        
        var urlRequest = URLRequest(url: request.url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: timeout)
        urlRequest.allHTTPHeaderFields = request.headers
        urlRequest.httpBody = request.body
        urlRequest.httpMethod = request.httpVerb.rawValue
        
        return urlRequest
    }
    
    // Data Request
    func request<T: Codable>(_ requestModel: APIRequest, onCompletion: @escaping (_ promise: Promise<T>) -> ()) {
        session.dataTask(with: prepareSessiomRequest(requestModel)) { (data, response, error) in
            if let apiError = error {
                DispatchQueue.main.async {
                    onCompletion(Promise.failure(.custom(messge: apiError.localizedDescription)))
                    return
                }
            }
            
            // Logging
            if let responseData = data {
                print(String(data: responseData, encoding: .utf8) ?? "NO JSON")
            }
            if let responseData = data,
                let jsonResponse: T? = JSONParser().get(withData: responseData),
                let json = jsonResponse {
                DispatchQueue.main.async {
                    onCompletion(Promise.success(json))
                }
            } else {
                print("JSON Parsing Failed 🐮. Please Check Your Response Model Class.")
                DispatchQueue.main.async {
                    if let response = response as? HTTPURLResponse, response.statusCode == 401 {
                        onCompletion(Promise.failure(.custom(messge: "Authorization Failure")))
                        //NSError(domain: "AUTH_FAILED", code: 401, userInfo: [NSLocalizedDescriptionKey: AppError.serverFailed.getMessage]))
                    } else {
                        onCompletion(Promise.failure(.custom(messge: "API Server Error")))
                    }
                }
            }
        }.resume()
    }

}
