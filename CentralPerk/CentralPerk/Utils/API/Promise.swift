//
//  Promise.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 16/03/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation

enum Promise<Data> {
    case failure(AppError)
    case success(Data)
}
