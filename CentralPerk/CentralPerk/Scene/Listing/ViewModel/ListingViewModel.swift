//
//  ListingViewModel.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 21/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation

class ListingViewModel: BaseViewModel {
    
    var handleStatus: ((_ user: UserModel) -> Void)?
    var handlerSuccess: ((_ friends: [UserModel]) -> Void)?
    var handlerError: ((_ error: AppError) -> Void)?
    
    var userProfile: UserModel!
    var friendsUserModels: [UserModel] = []
    
    init(user: UserModel) {
        self.userProfile = user
    }
    
    func updateLastSeen() {
        guard let uid = userProfile.uid else { return }
        let query = "users/\(uid)"
        RealtimeDatabaseManager.shared.update(child: query, data: ["lastSeen": Utility.getTimestamp()]) { (promise: Promise<Bool>) in
            print("Last Seen Updated for User: \(uid)")
        }
    }
    
    func requestForFriendsData() {
        guard let uid = userProfile.uid else { return }
        
        let query = "friends/\(uid)"
        self.handlerViewState?(true)
        RealtimeDatabaseManager.shared.read(path: query) { (promise: Promise<NSDictionary>) in
            self.handlerViewState?(false)
            switch promise {
            case .failure(let error):
                self.handlerError?(error)
            case .success(let data):
                if let friends = data as? [String: Any] {
                    self.getFriendsData(friends: friends)
                } else {
                    // No Friends associated with user
                    self.handlerSuccess?([])
                }
            }
        }
    }
    
    func getFriendsStatusListner(friends: [UserModel]) {
        friends.forEach { (user) in
            guard let uid = user.uid else { return }
            let query = "users/\(uid)/status"
            print(query)
            RealtimeDatabaseManager.shared.readWithListner(path: query) { (promise: Promise<String>) in
                switch promise {
                case .failure(let error):
                    print("Status Error: \(error.text)")
                case .success(let status):
                    user.status = UserStatus(rawValue: status) ?? .offline
                    self.handlerSuccess?(friends)
                }
            }
        }
    }
    
    func getFriendsData(friends: [String: Any]) {
        var users: [UserModel] = []
        for (_, friend) in friends {
            if let friendDict = friend as? [String: String],
                let friendUID = friendDict["friendId"],
                let chatId = friendDict["chatId"] {
                
                RealtimeDatabaseManager.shared.readWithListner(path: "users/\(friendUID)") { (promise: Promise<NSDictionary>) in
                    switch promise {
                    case .failure(let error):
                        self.handlerError?(error)
                    case .success(let data):
                        if let userModel = UserModel(dict: data) {
                            userModel.setChatId(id: chatId)
                            users.append(userModel)
                            
                            // Chat Subscribe
                            self.subscribeUser(chatId: chatId)
                            
                            //FIXME
                            if users.count == friends.count {
                                self.getFriendsStatusListner(friends: users)
                            }
                        } else {
                            self.handlerSuccess?(users)
                            self.handlerError?(.custom(messge: "Can't Parse into UserModel"))
                        }
                    }
                }
            }
        }
    }
    
    func subscribeUser(chatId: String) {
        FirebaseMessagingManager().subscribe(topic: .chat(conversationId: chatId))
    }
    
}
