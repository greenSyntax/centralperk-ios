//
//  UserTableViewCell.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 20/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

protocol FriendTableViewCellDelegate: class {
    func onClickedProfilePicture(image: UIImage?)
}

class FriendTableViewCell: UITableViewCell {

    @IBOutlet weak var imageFriend: UIImageView!
    @IBOutlet weak var lableTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var viewCount: UIView!
    @IBOutlet weak var labelCount: UILabel!
    @IBOutlet weak var labelIcon: UILabel!
    @IBOutlet weak var viewForImage: UIView!
    
    weak var delegate: FriendTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initialize()
    }
    
    private func initialize() {
        self.setGesture()
        self.backgroundColor = UIColor.white
        self.selectionStyle = .none
        self.imageFriend.layer.borderWidth = 1.0
        self.imageFriend.layer.borderColor = UIColor.lightGray.cgColor
        self.imageFriend.layer.cornerRadius = 10
        self.viewCount.layer.cornerRadius = self.viewCount.bounds.height / 2
    }
    
    func configure(user: UserModel) {
        self.lableTitle.text = user.name
        self.setStatus(user)
        self.setUnreadMessageCount(user)
        if let imagePath = user.profilePictureURL {
            imageView?.image = #imageLiteral(resourceName: "img_user")
            RemoteImage.get(imagePath, imageView: self.imageFriend) { (isSuccess) in
                print("Successfully Downloaded : \(imagePath) for User \(user.name)")
            }
        } else {
            imageView?.image = #imageLiteral(resourceName: "img_user")
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.lableTitle.text = ""
        self.labelDescription.text = ""
        self.imageFriend.image = nil
    }
    
    private func setGesture() {
        let tapOnImage = UITapGestureRecognizer(target: self, action: #selector(clickedOnImage))
        self.viewForImage.addGestureRecognizer(tapOnImage)
    }
    
    @objc func clickedOnImage() {
        delegate?.onClickedProfilePicture(image: self.imageFriend?.image)
    }
    
    private func setUnreadMessageCount(_ user: UserModel) {
        if let count = user.unreadMessageCount, count > 0 {
            self.labelCount.isHidden = false
            self.viewCount.isHidden = false
            self.labelCount.text = String(describing: count)
        } else {
            self.viewCount.isHidden = true
            self.labelCount.isHidden = true
        }
    }
    
    private func prepareOnlineIcon() {
        self.labelIcon.isHidden = false
        labelIcon.textColor = UIColor.appGreenColor
        labelIcon.font = UIFont.fontAwesome(ofSize: 10, style: .solid)
        labelIcon.text = String.fontAwesomeIcon(name: .circle)
    }
    
    private func setStatus(_ user: UserModel) {
        if user.status == .online {
            self.labelDescription.text = user.status.rawValue.capitalized
            self.prepareOnlineIcon()
        } else {
            self.labelIcon.isHidden = true
            if let lastSeen = user.lastSeen, let lastSeenInDouble = Double(lastSeen) {
                let date = Utility.getDateFromTimestamp(timeStamp: lastSeenInDouble)
                self.labelDescription.text = "Last Seen at \(date.getFormattedDate())"
            }
        }
    }
}
