//
//  ListingViewController.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 20/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

class ListingViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonMenu: UIButton!
    @IBOutlet weak var buttonAddFriends: UIButton!
    @IBOutlet weak var labelNavigationTitle: UILabel!
    
    weak var delegate: MenuBarDelegate?
    
    var viewModel: ListingViewModel!
    var dataSource: [UserModel] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialize()
        self.bindViewModel()
        self.setupTableView()
        self.registerNotifications()
        self.viewModel.requestForFriendsData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.updateLastSeen()
    }
    
    deinit {
        self.removeNotifications()
    }
    
    func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateFriendsList), name: Notification.Name("UpdateFriendsList"), object: nil)
    }
    
    func removeNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func updateFriendsList() {
        self.refreshView()
    }
    
    override func refreshView() {
        self.viewModel.requestForFriendsData()
    }
    
    func initialize() {
        self.labelNavigationTitle.text = AppText.appNameFormatted
        self.labelNavigationTitle.textColor = UIColor.darkGray
        
        // Add
        buttonAddFriends.tintColor = UIColor.darkGray
        buttonAddFriends.titleLabel?.font = UIFont.fontAwesome(ofSize: 20, style: .solid)
        buttonAddFriends.setTitle(String.fontAwesomeIcon(name: .plus), for: .normal)
        
        // Menu
        buttonMenu.tintColor = UIColor.darkGray
        buttonMenu.titleLabel?.font = UIFont.fontAwesome(ofSize: 20, style: .solid)
        buttonMenu.setTitle(String.fontAwesomeIcon(name: .bars), for: .normal)
    }
    
    func setupTableView() {
        registerTableView()
        
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorInset = .zero
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    func registerTableView() {
        self.tableView.register(UINib(nibName: String(describing: FriendTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: FriendTableViewCell.self))
    }
    
    func showEmptyView() {
        let containerView = UIView()
        self.tableView.backgroundView = containerView
        self.showNoData(type: .noFriends, containerView: containerView)
    }
    
    func bindViewModel() {
        self.viewModel.handlerSuccess = { [weak self] (_ friends: [UserModel]) in
            if friends.isEmpty {
                self?.dataSource.removeAll()
                self?.showEmptyView()
                return
            }
            
            // Update Listing View
            self?.tableView.backgroundView = UIView()
            self?.tableView.backgroundColor = UIColor.white
            self?.dataSource.removeAll()
            self?.dataSource = friends
        }
        
        self.viewModel.handlerViewState = { [weak self] (_ isLoading: Bool) in
            self?.state = isLoading ? ViewState.loading : ViewState.done
        }
        
        self.viewModel.handleStatus = { [weak self] (user: UserModel) in
            self?.tableView.reloadData()
        }
        
        self.viewModel.handlerError = { [weak self] (_ error: AppError) in
            //self?.state = .error(error: error)
            self?.showEmptyView()
        }
    }
    
    @IBAction func buttonAddFriendsAction(_ sender: Any) {
        if let vc = InviteViewController.loadViewController() as? InviteViewController {
            vc.viewModel = InviteViewModel(user: viewModel.userProfile)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func buttonMenuAction(_ sender: Any) {
        delegate?.onMenuAction()
    }
    
}

extension ListingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FriendTableViewCell.self)) as? FriendTableViewCell {
            cell.delegate = self
            cell.configure(user: dataSource[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        routeToChat(friend: dataSource[indexPath.row], user: viewModel.userProfile)
    }
    
}

extension ListingViewController {
    
    func routeToChat(friend: UserModel, user: UserModel) {
        if let vc = ChatViewController.loadViewController() as? ChatViewController {
            vc.viewModel = ChatViewModel(friend: friend, user: user)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

extension ListingViewController: FriendTableViewCellDelegate {
    func onClickedProfilePicture(image: UIImage?) {
        guard let image = image else { return  }
        if let vc = PreviewViewController.loadViewController() as? PreviewViewController {
            vc.image = image
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
}
