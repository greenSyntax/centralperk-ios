//
//  WebViewController.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 13/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: BaseViewController {

    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var labelTitle: UILabel!
    
    var data: (url: URL, title: String)!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initializeView()
        loadWeb()
    }
    
    func initializeView() {
        self.setCloseButton(self.buttonBack)
        self.labelTitle.text = data.title
    }
    
    func loadWeb() {
        let request = URLRequest(url: self.data.url)
        webView.navigationDelegate = self
        webView.load(request)
    }
    
    @IBAction func actionBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}

extension WebViewController: WKNavigationDelegate {
    
}
