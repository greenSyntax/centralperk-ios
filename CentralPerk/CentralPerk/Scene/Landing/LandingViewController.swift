//
//  LandingViewController.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 20/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

class LandingViewController: BaseViewController {

    @IBOutlet weak var buttonOnboarding: UIButton!
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var imageBackground: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var labelFooterText: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initializeView()
    }
    
    func initializeView() {
        self.addPrivacyView()
        self.view.backgroundColor = UIColor.white
        self.imageBackground.backgroundColor = UIColor(patternImage: UIImage(named: "img_bg")!)
        self.buttonOnboarding.setTitle(AppText.onboardingButtonText, for: .normal)
        self.labelTitle.text = AppText.appName
        self.labelDescription.text = AppText.appDescription
        self.labelFooterText.text = AppText.landingFooterText
    }
    
    func addPrivacyView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tappedOnPrivacyPolicy))
        viewFooter.addGestureRecognizer(tap)
    }
    
    @objc func tappedOnPrivacyPolicy() {
        if let vc = WebViewController.loadViewController() as? WebViewController {
            vc.data = (url: URL(string: AppConfig.privacyURL)!, title: AppText.privacyPolicy)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func buttonOnboardingAction(_ sender: Any) {
        if let vc = LoginViewController.loadViewController() as? LoginViewController {
            vc.viewModel = AuthViewModel()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
