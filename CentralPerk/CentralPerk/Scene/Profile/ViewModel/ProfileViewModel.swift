//
//  ProfileViewModel.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 27/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation
import UIKit

class ProfileViewModel: BaseViewModel {
    
    var user: UserModel!
    var isEditable: Bool!
    var imagePicker = ImagePickerWrapper()
    
    var hanlderSuccess: ((_ hasUpdated: Bool) -> Void)?
    var handlerError: ((_ error: AppError) -> Void)?
    var handleImageUpdate: ((_ image: UIImage) -> Void)?
    
    init(user: UserModel, isEditable: Bool = false) {
        self.user = user
        self.isEditable = isEditable
    }
    
    func setData(name: String, email: String, mobile: String) {
        
        user.email = email
        user.name = name
        user.mobile = mobile
        
        UserService().updateProfile(user: user) { (promise: Promise<Bool>) in
            switch promise {
            case .failure(let error):
                self.handlerError?(error)
            case .success(let hasUpdated):
                self.hanlderSuccess?(hasUpdated)
            }
        }
    }
    
    func updateImage(_ view: UIViewController) {
        imagePicker.show(view, delegate: self)
    }
    
}

extension ProfileViewModel: ImagePickerWrapperDelegate {
    func didSelectedImage(_ image: UIImage?) {
        guard let uid = user.uid else { return }
        if let image = image {
            self.handleImageUpdate?(image)
            
            StorageManager().upload(image: image) { (url, error) in
                RealtimeDatabaseManager.shared.update(child: "users/\(uid)", data: ["profilePictureURL": url?.absoluteString ?? ""]) { (promise: Promise<Bool>) in
                    switch promise {
                    case .failure(let error):
                        self.handlerError?(error)
                    case .success(let isUpdated):
                        self.hanlderSuccess?(isUpdated)
                    }
                }
            }
        }
    }
}
