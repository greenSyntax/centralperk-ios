//
//  ProfileViewController.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 27/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class ProfileViewController: BaseViewController {

    @IBOutlet weak var imageProfilePicture: UIImageView!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var textFieldFullName: JVFloatLabeledTextField!
    @IBOutlet weak var textFieldEmail: JVFloatLabeledTextField!
    @IBOutlet weak var textFieldMobile: JVFloatLabeledTextField!
    @IBOutlet weak var buttonEdit: UIButton!
    @IBOutlet weak var buttonEditPicture: UIButton!
    
    var viewModel: ProfileViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bindViewModel()
        initializeView()
    }
    
    func initializeView() {
        self.setCloseButton(buttonBack)
        self.populateProfileData()
        self.registerOutsideTap()
        self.setViewEditable()
        self.setEditImage()
        self.buttonEditPicture.isHidden = !viewModel.isEditable
    }
    
    func populateProfileData() {
        self.textFieldFullName.text = viewModel.user.name
        self.textFieldEmail.text = viewModel.user.email
        self.textFieldMobile.text = viewModel.user.mobile
        if let imagePath = viewModel.user.profilePictureURL {
            RemoteImage.get(imagePath, imageView: self.imageProfilePicture) { (isLoaded) in
                print("Image Downloaded: \(imagePath)")
            }
        }
    }
    
    func setEditImage() {
        buttonEditPicture.layer.cornerRadius = 4.0
        buttonEditPicture.backgroundColor = UIColor.groupTableViewBackground
        buttonEditPicture.tintColor = UIColor.darkGray
        buttonEditPicture.titleLabel?.font = UIFont.fontAwesome(ofSize: 20, style: .solid)
        buttonEditPicture.setTitle(String.fontAwesomeIcon(name: .pencilAlt), for: .normal)
    }
    
    func setViewEditable() {
        self.textFieldEmail.isEnabled = false
        self.textFieldEmail.textColor = viewModel.isEditable ? UIColor.darkGray : UIColor.black
        
        self.textFieldMobile.isEnabled = viewModel.isEditable
        self.textFieldFullName.isEnabled = viewModel.isEditable
        self.buttonEdit.isHidden = !viewModel.isEditable
    }
    
    func bindViewModel() {
        self.viewModel.hanlderSuccess = { (_ updated: Bool) in
            self.showToast(message: updated ? "Successfully Updated" : "Pleas try again")
        }
        
        self.viewModel.handlerError = { [weak self] (_ error: AppError) in
            self?.state = .error(error: error)
        }
        
        self.viewModel.handlerViewState = { [weak self] (_ isLoading: Bool) in
            self?.state = isLoading ? ViewState.loading : ViewState.done
        }
        
        self.viewModel.handleImageUpdate = { [weak self] (_ image: UIImage) in
            self?.imageProfilePicture.image = image
        }
    }
    
    @IBAction func actionButtonEditPicture(_ sender: Any) {
        viewModel.updateImage(self)
    }
    
    @IBAction func buttonBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonEditAction(_ sender: Any) {
        guard let name = textFieldFullName.text, !name.isEmpty else { return }
        guard let email = textFieldEmail.text, !email.isEmpty else { return }
        guard let mobile = textFieldMobile.text, !mobile.isEmpty else { return }
        
        viewModel.setData(name: name, email: email, mobile: mobile)
    }

}
