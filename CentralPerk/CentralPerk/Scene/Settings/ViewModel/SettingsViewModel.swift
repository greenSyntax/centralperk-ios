//
//  SettingsViewModel.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 11/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation
import UIKit

struct PreferenceModel {
    var type: SettingItem
    var isEnabled: Bool
    
    init(type: SettingItem, isEnabled: Bool) {
        self.type = type
        self.isEnabled = isEnabled
    }
}

class SettingsViewModel: BaseViewModel {
   
    var handlerSuccess: ((_ user: UserModel) -> Void)?
    var handlerError: ((_ error: AppError) -> Void)?
    var handlerToast: ((_ text: String) -> Void)?
    
    var user: UserModel!
    var preferences: [PreferenceModel] = []
    
    init(user: UserModel) {
        self.user = user
    }
    
    private func preparePreferences() {
        self.preferences.append(PreferenceModel(type: .pushNotification, isEnabled: self.user.isPushNotification ?? true))
        //self.preferences.append(PreferenceModel(type: .publicRoom, isEnabled: self.user.isPublicRoom ?? true))
        //self.preferences.append(PreferenceModel(type: .status, isEnabled: self.user.isLastSeen ?? true))
    }
    
    func getPrefrenceData() {
        guard let uid = user.uid else { return }
        self.handlerViewState?(true)
        let query = "users/\(uid)"
        RealtimeDatabaseManager.shared.read(path: query) { (promise: Promise<NSDictionary>) in
            self.handlerViewState?(false    )
            switch promise {
            case .failure(let error):
                self.handlerError?(error)
            case .success(let dict):
                self.user = UserModel(dict: dict)
                self.preparePreferences()
                self.handlerSuccess?(self.user)
            }
        }
    }
    
    func getPrefernceStatus(type: SettingItem) -> Bool {
        for (_, value) in self.preferences.enumerated() {
            if value.type == type {
                return value.isEnabled
            }
        }
        return false
    }
    
    func localState(type: SettingItem, isOn: Bool) {
        switch type {
        case .pushNotification:
            PreferenceManager.shared.isPushNotification = isOn
        default:
            print("Unhandled State for Settings State")
        }
    }
    
    func setPreferenceStatus(type: SettingItem, isEnabled: Bool) {
        guard let uid = user.uid else { return }
        self.localState(type: type, isOn: isEnabled)
        let query = "users/\(uid)/"
        RealtimeDatabaseManager.shared.update(child: query, data: [type.alias(): isEnabled]) { (promise: Promise<Bool>) in
            switch promise {
            case .failure(let error):
                self.handlerError?(error)
            case .success(let hasUpdated):
                if hasUpdated {
                    self.handlerToast?("Successfully Updated")
                } else {
                    self.handlerToast?("Failed while updating")
                }
            }
        }
    }
    
    func setLogout() {
        Utility.showAlert(title: "Logout", description: "Do You want to logout from this device ?", positiveText: "Yes", negativeText: "No", onPositiveAction: {
            UserService().updateStatus(status: .offline)
            PreferenceManager.shared.clear()
            self.rouetToLandingScreen()
        }) {
            //Cancel
        }
    }
    
    func setDeactivateAccount() {
        Utility.showAlert(title: "Deactive Account", description: "Do you want to remove your complete data ?", positiveText: "Yes", negativeText: "No", onPositiveAction: {
            self.handlerToast?("This is Under Development.")
        }) {
            // Cencel
        }
    }
    
    func rouetToLandingScreen() {
        let vc = LandingViewController.loadViewController()
        let navigationController = UINavigationController(rootViewController: vc)
        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }
    
}
