//
//  SettingsViewController.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 11/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

class SettingsViewController: BaseViewController {

    @IBOutlet weak var buttonMenu: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: SettingsViewModel!
    weak var delegate: MenuBarDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bindViewModel()
        initializeView()
        regiseterCell()
        initializeTableView()
        getData()
    }
    
    func setMenuButton() {
        buttonMenu.tintColor = UIColor.darkGray
        buttonMenu.titleLabel?.font = UIFont.fontAwesome(ofSize: 20, style: .solid)
        buttonMenu.setTitle(String.fontAwesomeIcon(name: .bars), for: .normal)
    }
    
    func initializeView() {
        setMenuButton()
    }
    
    func getData() {
        viewModel.getPrefrenceData()
    }
    
    func regiseterCell() {
        tableView.register(UINib(nibName: String(describing: SettingTableCell.self), bundle: nil), forCellReuseIdentifier: String(describing: SettingTableCell.self))
        tableView.register(UINib(nibName: String(describing: SettingsButtonCell.self), bundle: nil), forCellReuseIdentifier: String(describing: SettingsButtonCell.self))
    }
    
    func initializeTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorInset = .zero
        tableView.allowsSelection = false
        tableView.tableFooterView = UIView()
    }
    
    func bindViewModel() {
        
        viewModel.handlerSuccess = { [weak self] (_user: UserModel) in
            self?.tableView.reloadData()
        }
        
        viewModel.handlerError = { [weak self] (_ error: AppError) in
            self?.state = .error(error: error)
        }
        
        self.viewModel.handlerViewState = { [weak self] (_ isLoading: Bool) in
            self?.state = isLoading ? ViewState.loading : ViewState.done
        }
        
        self.viewModel.handlerToast = { [weak self] (_ text: String) in
            self?.showToast(message: text)
        }
    }
    
    @IBAction func actionMenuButton(_ sender: Any) {
        delegate?.onMenuAction()
    }

}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SettingItem.allCases.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type = SettingItem.allCases[indexPath.row]
        
        if type.actionType() == .toggle {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SettingTableCell.self)) as! SettingTableCell
            cell.delegate = self
            
            cell.configure(type, isOn: viewModel.getPrefernceStatus(type: type))
            return cell
        } else if type.actionType() == .button {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SettingsButtonCell.self)) as! SettingsButtonCell
            cell.delegate = self
            cell.configure(type: type)
            return cell
        }
        
        return UITableViewCell()
    }
    
}

extension SettingsViewController: SettingTableCellDelegate {
    func onChangeToogleSwitch(type: SettingItem, isOn: Bool) {
        viewModel.setPreferenceStatus(type: type, isEnabled: isOn)
    }
}

extension SettingsViewController: SettingsButtonCellDelegate {
    func onSettingsButtonAction(type: SettingItem) {
        switch type {
        case .logout:
            viewModel.setLogout()
        case .deactivate:
            viewModel.setDeactivateAccount()
        default:
            print("Unhandled Case")
        }
    }
}
