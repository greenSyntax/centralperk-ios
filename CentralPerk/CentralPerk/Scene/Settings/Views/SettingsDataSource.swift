//
//  SettingsDataSource.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 12/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation

enum SettingsAction {
    case button
    case toggle
}

enum SettingItem: CaseIterable {
    case pushNotification
    case deactivate
    case logout
    
    func alias() -> String {
        switch self {
        case .pushNotification:
            return "isPushNotification"
        case .deactivate:
            return "deactivate"
        case .logout:
            return "logout"
        }
    }
    
    func getData() -> (title: String, description: String) {
        switch self {
        case .pushNotification:
            return (title: "Push Notification", description: "Send Notifications for messages")
        case .deactivate:
            return (title: "Deactivate Your Account", description: "Deactive")
        case .logout:
            return (title: "Logout From Device", description: "Logout")
        }
    }
    
    func actionType() -> SettingsAction {
        switch self {
        case .pushNotification:
            return SettingsAction.toggle
        case .deactivate:
            return SettingsAction.button
        case .logout:
            return SettingsAction.button
        }
    }
}
