//
//  SettingsButtonCell.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 12/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

protocol SettingsButtonCellDelegate: class {
    func onSettingsButtonAction(type: SettingItem)
}

class SettingsButtonCell: UITableViewCell {

    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var buttonAction: UIButton!
    @IBOutlet weak var labelRightArrow: UILabel!
    
    weak var delegate: SettingsButtonCellDelegate?
    private var type: SettingItem!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initializeView()
    }
    
    private func initializeView() {
        labelRightArrow.textColor = UIColor.gray
        labelRightArrow.font = UIFont.fontAwesome(ofSize: 16, style: .solid)
        labelRightArrow.text = String.fontAwesomeIcon(name: .angleRight)
    }
    
    func configure(type: SettingItem) {
        self.type = type
        
        labelText.text = type.getData().title
        //buttonAction.setTitle(type.getData().description, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func buttonActionClick(_ sender: Any) {
        delegate?.onSettingsButtonAction(type: self.type)
    }

}
