//
//  SettingTableCell.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 11/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

protocol SettingTableCellDelegate: class {
    func onChangeToogleSwitch(type: SettingItem, isOn: Bool)
}

class SettingTableCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var toggleSwitch: UISwitch!
    
    weak var delegate: SettingTableCellDelegate?
    
    private var type: SettingItem!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
    
    func initializeView() {
        toggleSwitch.isOn = false
    }

    func configure(_ data: SettingItem, isOn: Bool) {
        self.type = data
        
        labelTitle.text = data.getData().title
        labelDescription.text = data.getData().description
        toggleSwitch.isOn = isOn
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func actionToggleSwitch(_ sender: Any) {
        delegate?.onChangeToogleSwitch(type: type, isOn: toggleSwitch.isOn)
    }
    
}
