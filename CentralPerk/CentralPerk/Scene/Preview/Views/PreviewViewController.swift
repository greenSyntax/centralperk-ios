//
//  PreviewViewController.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 13/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

class PreviewViewController: BaseViewController {
    
    @IBOutlet weak var buttonClose: UIButton!
    @IBOutlet weak var viewImage: UIImageView!
    
    var image: UIImage!
    
    //TODO:
    //var url: URL!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initializeView()
    }
    
    func setupBack() {
        buttonClose.backgroundColor = UIColor.groupTableViewBackground
        self.setCloseButton(self.buttonClose, color: UIColor.black)
        buttonClose.layer.cornerRadius = 4.0
    }
    
    func initializeView() {
        self.setupBack()
        self.viewImage.image = image
    }
    
    @IBAction func actionButtonClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
