//
//  InviteViewController.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 20/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

class InviteViewController: BaseViewController {

    @IBOutlet weak var buttonClose: UIButton!
    @IBOutlet weak var buttonShareInvitation: UIButton!
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    
    var viewModel: InviteViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialize()
    }
    
    func initialize() {
        self.labelTitle.text = AppText.inviteTitle
        self.labelDescription.text = AppText.inviteDescription
        self.buttonShareInvitation.setTitle(AppText.inviteShareLink, for: .normal)
        
        self.setBackButton(self.buttonClose)
    }
    
    @IBAction func buttonCloseAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonShareInvite(_ sender: Any) {
        if let link = viewModel.getInvitationLink() {
            ShareIntentManager.shareViaActivity(link)
        }
    }
    
}
