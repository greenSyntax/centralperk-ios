//
//  InviteViewModel.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 21/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation

class InviteViewModel: BaseViewModel {
    
    var user: UserModel!
    
    init(user: UserModel) {
        self.user = user
    }
    
    func getData() {
        
    }
 
    func getInvitationLink() -> URL? {
        if let uid = user.uid,
            let link = URL(string: Utility.generateInvitationURL(uid: uid)) {
            print("Invitation URL: \(link.absoluteString)")
            return link
        }
        return nil
    }
}
