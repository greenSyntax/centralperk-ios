//
//  AboutViewController.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 11/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    
    @IBOutlet weak var buttonMenu: UIButton!
    @IBOutlet weak var imageFacebook: UIImageView!
    @IBOutlet weak var imageTwitter: UIImageView!
    @IBOutlet weak var labelAppName: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelFooterText: UILabel!
    
    
    weak var delegate: MenuBarDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initializeView()
        setMenuButton()
        tapRegister()
    }

    func initializeView() {
        self.labelAppName.text = AppText.appName
        self.labelDescription.text = AppText.appDescription
        self.labelFooterText.text = AppText.author
    }
    
    func setMenuButton() {
        buttonMenu.tintColor = UIColor.darkGray
        buttonMenu.titleLabel?.font = UIFont.fontAwesome(ofSize: 20, style: .solid)
        buttonMenu.setTitle(String.fontAwesomeIcon(name: .bars), for: .normal)
    }
    
    func tapRegister() {
        let tapFacebook = UITapGestureRecognizer(target: self, action: #selector(tappedOnFacebook))
        self.imageFacebook.addGestureRecognizer(tapFacebook)
        
        let tapTwitter = UITapGestureRecognizer(target: self, action: #selector(tappedOnTwitter))
        self.imageTwitter.addGestureRecognizer(tapTwitter)
    }
    
    @objc func tappedOnFacebook() {
        
    }
    
    @objc func tappedOnTwitter() {
        
    }
    
    @IBAction func actionMenuButton(_ sender: Any) {
        delegate?.onMenuAction()
    }
    
}
