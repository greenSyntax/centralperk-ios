//
//  ImageMessageCell.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 13/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

protocol ImageMessageCellDelegate: class {
    func onClickImageView(model: ChatMessageModel)
}

//TODO: Make a Base Message Cell
class ImageMessageCell: UITableViewCell {

    @IBOutlet weak var imageMessage: UIImageView!
    @IBOutlet weak var labelTimestamp: UILabel!
    @IBOutlet weak var viewParent: UIView!
    @IBOutlet weak var labelSender: UILabel!
    
    @IBOutlet weak var parentLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var parentTrailingConstraint: NSLayoutConstraint!
    
    private var isIncoming: Bool = false
    private var model: ChatMessageModel!
    
    weak var delegate: ImageMessageCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
    
    func initializeView() {
        self.backgroundColor = UIColor.white
        viewParent.layer.borderWidth = 1.0
        viewParent.layer.borderColor = UIColor.gray.cgColor
        viewParent.layer.cornerRadius = 10.0
        imageMessage.layer.cornerRadius = 10.0
    }
    
    func configure(model: ChatMessageModel) {
        self.model = model
        self.isIncoming = model.isIncoming
        guard let base64 = model.imageData, let imageData = Data(base64Encoded: base64) else { return }
        if model.isIncoming { self.labelSender.text = model.sender }
        self.isIncoming = model.isIncoming
        self.imageMessage.image = UIImage(data: imageData)
        self.labelTimestamp.text = model.timeStamp.getFormattedForMessages()
        
        self.setupTapArea()
    }

    private func setupTapArea() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tappedOnImage))
        self.viewParent.addGestureRecognizer(tap)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if isIncoming {
            labelTimestamp.textAlignment = .left
            parentTrailingConstraint.isActive = false
            parentLeadingConstraint.isActive = true
            labelSender.textAlignment = .left
        } else {
            labelTimestamp.textAlignment = .right
            parentLeadingConstraint.isActive = false
            parentTrailingConstraint.isActive = true
            labelSender.textAlignment = .right
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        
        labelSender.text = ""
        labelTimestamp.text = ""
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @objc func tappedOnImage() {
        delegate?.onClickImageView(model: model)
    }

}
