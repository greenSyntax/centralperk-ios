//
//  ChatMessageCell.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 16/03/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

class ChatMessageCell: UITableViewCell {

    let labelMessage = UILabel()
    let viewBackground = UIView()
    let labelSentTimstamp = UILabel()
    
    var model: ChatMessageModel!
    var constaints: [NSLayoutConstraint] = []
    var needLayouting: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setup()
    }
    
    private func setup() {
        setBackgroundView()
        setLabel()
        setFont()
        setConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(model: ChatMessageModel) {
        labelMessage.text = model.message
        labelSentTimstamp.text = model.timeStamp.getFormattedForMessages()
        setColor(model)
        changeOrdientation(isIncoming: model.isIncoming)
        
    }
    
    private func setFont() {
        labelMessage.font = UIFont(name: "Avenir-Medium", size: 16.0)
        labelSentTimstamp.font = UIFont(name: "Avenir-Medium", size: 10.0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        print("Layout Subviews ....")
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        print("Layout If Needed ...")
    }
    
    private func setColor(_ model: ChatMessageModel) {
        viewBackground.backgroundColor = model.isIncoming ? UIColor.groupTableViewBackground : UIColor.appGreenColor
        labelMessage.textColor =  model.isIncoming ? UIColor.darkGray : UIColor.white
        labelSentTimstamp.textColor = UIColor.lightGray
    }
    
    private func setLabel() {
        addSubview(labelMessage)
        labelMessage.numberOfLines = 0
        labelMessage.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(labelSentTimstamp)
        labelSentTimstamp.numberOfLines = 0
        labelSentTimstamp.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func setBackgroundView() {
        viewBackground.backgroundColor = UIColor.yellow
        viewBackground.layer.cornerRadius = 10.0
        addSubview(viewBackground)
        viewBackground.translatesAutoresizingMaskIntoConstraints = false
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()

    }
    
    private func setConstraints() {
        
        //Message Label
        self.constaints = [
            labelMessage.topAnchor.constraint(equalTo: topAnchor, constant: 24),
            labelMessage.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -24),
            labelMessage.widthAnchor.constraint(lessThanOrEqualToConstant: 250),
            labelMessage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            labelSentTimstamp.leadingAnchor.constraint(equalTo: viewBackground.leadingAnchor, constant: 6),
            
            labelSentTimstamp.topAnchor.constraint(equalTo: viewBackground.bottomAnchor, constant: 8),
            labelSentTimstamp.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 8),
            
            viewBackground.topAnchor.constraint(equalTo: labelMessage.topAnchor, constant: -12),
            viewBackground.bottomAnchor.constraint(equalTo: labelMessage.bottomAnchor, constant: 12),
            viewBackground.leadingAnchor.constraint(equalTo: labelMessage.leadingAnchor, constant: -12),
            viewBackground.trailingAnchor.constraint(equalTo: labelMessage.trailingAnchor, constant: 12)
        ]
        
        NSLayoutConstraint.activate(self.constaints)
    }
    
    private func changeOrdientation(isIncoming: Bool) {
        if isIncoming {
            //viewBackground.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24).isActive = true
            //labelSentTimstamp.leadingAnchor.constraint(equalTo: viewBackground.leadingAnchor, constant: 6).isActive = true
        } else {
            //viewBackground.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24).isActive = true
            //labelSentTimstamp.trailingAnchor.constraint(equalTo: viewBackground.trailingAnchor, constant: -6).isActive = true
        }
    }

}
