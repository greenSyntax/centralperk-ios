//
//  MessageCell.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 27/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

    @IBOutlet weak var labelSender: UILabel!
    @IBOutlet weak var labelMessageText: UILabel!
    @IBOutlet weak var labelTimestamp: UILabel!
    @IBOutlet weak var viewParent: UIView!
    
    @IBOutlet weak var constraintMessageLeading: NSLayoutConstraint!
    @IBOutlet weak var consraintMessageTrailing: NSLayoutConstraint!
    
    var isIncoming: Bool = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initialize()
    }
    
    private func initialize() {
        viewParent.layer.cornerRadius = 10
        self.backgroundColor = UIColor.white
    }
    
    private func setColor(_ isIncoming: Bool) {
        if isIncoming {
            self.viewParent.backgroundColor = UIColor.appLightGray
            self.labelMessageText.textColor = UIColor.darkGray
        } else {
            self.viewParent.backgroundColor = UIColor.appGreenColor
            self.labelMessageText.textColor = UIColor.white
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if isIncoming {
            labelSender.textAlignment = .left
            labelTimestamp.textAlignment = .left
            consraintMessageTrailing.isActive = false
            constraintMessageLeading.isActive = true
        } else {
            labelSender.textAlignment = .right
            labelTimestamp.textAlignment = .right
            constraintMessageLeading.isActive = false
            consraintMessageTrailing.isActive = true
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        labelSender.text = ""
        labelMessageText.text = ""
        labelTimestamp.text = ""
    }
    
    func configure(model: ChatMessageModel) {
        self.setColor(model.isIncoming)
        self.isIncoming = model.isIncoming
        if model.isIncoming { self.labelSender.text = model.sender }
        self.labelMessageText.text = model.message
        self.labelTimestamp.text = model.timeStamp.getFormattedForMessages()
    }

}
