//
//  ChatViewController.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 16/03/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

class ChatViewController: BaseViewController {

    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var viewTextField: UIView!
    @IBOutlet weak var buttonMore: UIButton!
    @IBOutlet weak var buttonAttachment: UIButton!
    @IBOutlet weak var textFieldTextMessages: UITextField!
    @IBOutlet weak var buttonSend: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    var isFirstTime: Bool = false
    var viewModel: ChatViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initalizeView()
        bindViewModel()
        viewModel.getData()
    }

    private func initalizeView() {
        registerCells()
        
        self.isFirstTime = true
        self.labelTitle.text = viewModel.friend.name ?? "User"
        self.registerOutsideTap()
        self.setBackButton(self.buttonBack)
        self.moreButton(self.buttonMore)
        self.addAttachmentButton()
        self.addSendButton()
        
        self.tableView.backgroundColor = UIColor.white
        //self.viewNavigation.backgroundColor = UIColor.appLightGray
        self.textFieldTextMessages.textColor = UIColor.darkGray
        self.viewTextField.layer.borderColor = UIColor.lightGray.cgColor.copy(alpha: 0.5)
        self.viewTextField.layer.borderWidth = 1.0
        self.viewTextField.layer.cornerRadius = viewTextField.bounds.height / 2
    }
    
    func bindViewModel() {
        
        viewModel.handlerSuccess = { (_ messages: [ChatMessageModel]) in
            
            if !messages.isEmpty {
                self.tableView.reloadData()
            }
            
            if let _ = self.tableView.indexPathsForVisibleRows?.last {
                //self.tableView.scrollToRow(at: index, at: .bottom, animated: true)
                self.tableView.layoutIfNeeded()
                self.tableView.scrollToBottom(animated: true)
            }
        }
        
        self.viewModel.handlerViewState = { [weak self] (_ isLoading: Bool) in
            guard let view = self?.tableView else { return }
            self?.state = isLoading ? ViewState.area(view: view) : ViewState.done
        }
            
        self.viewModel.handlerError = { [weak self] (_ error: AppError) in
            //self?.state = .error(error: error)
            print("Error: \(error.text)")
        }
        
        self.viewModel.hanldleUpdate = { [weak self] (_ isSuccess: Bool) in
            if isSuccess {
                self?.textFieldTextMessages.text = ""
            }
        }
        
        self.viewModel.handlerDismiss = { [weak self] () in
            NotificationCenter.default.post(name: Notification.Name("UpdateFriendsList"), object: nil)
            self?.navigationController?.popViewController(animated: true)
        }
    }
    
    private func addAttachmentButton() {
        buttonAttachment.tintColor = UIColor.darkGray
        buttonAttachment.titleLabel?.font = UIFont.fontAwesome(ofSize: 20, style: .solid)
        buttonAttachment.setTitle(String.fontAwesomeIcon(name: .paperclip), for: .normal)
    }
    
    private func addSendButton() {
        buttonSend.tintColor = UIColor.black
        buttonSend.titleLabel?.font = UIFont.fontAwesome(ofSize: 24, style: .solid)
        buttonSend.setTitle(String.fontAwesomeIcon(name: .arrowRight), for: .normal)
    }
    
    private func registerCells() {
        tableView.register(ChatMessageCell.self, forCellReuseIdentifier: String(describing: ChatMessageCell.self))
        tableView.register(UINib(nibName: String(describing: MessageCell.self), bundle: nil), forCellReuseIdentifier: String(describing: MessageCell.self))
        tableView.register(UINib(nibName: String(describing: ImageMessageCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ImageMessageCell.self))
        tableView.register(UINib(nibName: "DateView", bundle: nil), forHeaderFooterViewReuseIdentifier: "DateView")
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    @IBAction func actionBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonMoreAction(_ sender: Any) {
        showUserOptions()
    }
    
    @IBAction func buttonAttachmentAction(_ sender: Any) {
        viewModel.pickImage(view: self)
    }
    
    @IBAction func buttonSendAction(_ sender: Any) {
        guard let message = textFieldTextMessages.text, !message.isEmpty else { return }
        viewModel.sendMessages(text: message)
        dismissResponder()
    }
    
}

extension ChatViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.getNumberOfSections()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumberOfRows(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //print("Section: \(indexPath.section) Row: \(indexPath.row)")
        let vm = viewModel.getRowData(indexPath: indexPath)
        if let type = vm.type {
            switch type {
            case .image:
                if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ImageMessageCell.self)) as? ImageMessageCell {
                    cell.delegate = self
                    cell.configure(model: vm)
                    return cell
                }
            case .text:
                if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MessageCell.self)) as? MessageCell {
                    cell.configure(model: vm)
                    return cell
                }
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UINib(nibName: String(describing: DateView.self), bundle: nil).instantiate(withOwner: self, options: [:]).first as? DateView
        view?.configure(day: viewModel.getSectionData(section: section))
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}

extension ChatViewController {
    func showUserOptions() {
        let handlerOnProfile = {
            if let vc = ProfileViewController.loadViewController() as? ProfileViewController {
                vc.viewModel = ProfileViewModel(user: self.viewModel.friend)
                self.present(vc, animated: true, completion: nil)
            }
        }
        
        let handlerOnDelete = {
            Utility.showAlert(title: "Remove Friend", description: "Do You want to remove your friend from list?", positiveText: "Yes", negativeText: "No", onPositiveAction: {
                self.viewModel.removeFriend()
            }) {
                //Nothing
            }
        }
        
        Utility.showActionSheet(title: nil, description: nil, options: [("View Profile", handlerOnProfile), ("Remove Friend", handlerOnDelete)])
    }
}

extension ChatViewController {
    override func onKeyboardShown(_ keyboardSize: CGRect) {
        let value = keyboardSize.size.height
        let extra = UIDevice.hasNotch ? 70 : 0
        bottomConstraint.constant = -(value + CGFloat(extra))
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func onKeyboardHide() {
        bottomConstraint.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}

extension ChatViewController: ImageMessageCellDelegate {
    func onClickImageView(model: ChatMessageModel) {
        if let vc = PreviewViewController.loadViewController() as? PreviewViewController {
            guard let base64 = model.imageData, let imageData = Data(base64Encoded: base64) else { return }
            vc.image = UIImage(data: imageData)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
}
