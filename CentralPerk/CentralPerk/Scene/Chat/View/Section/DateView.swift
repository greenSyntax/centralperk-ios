//
//  DateView.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 16/03/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation
import UIKit

class DateView: UIView {
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    
    override func awakeFromNib() {
        viewContainer.layer.cornerRadius = viewContainer.bounds.height / 2
        viewContainer.backgroundColor = UIColor.appLightGray
    }
    
    func configure(day: String) {
        self.labelDate.text = day
    }
    
}
