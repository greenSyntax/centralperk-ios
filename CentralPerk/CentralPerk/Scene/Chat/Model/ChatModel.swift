//
//  ChatViewModel.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 16/03/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation

enum MessageType: String {
    case text
    case image
}

struct ChatMessageModel {
    
    var message: String?
    var timeStamp: Date
    var isIncoming: Bool
    var sender: String?
    var type: MessageType?
    var imageData: String?
    var timeComponent: (day: String, time: String)?
    
    init(message: String?, timeStamp: Date, isIncoming: Bool, imageData: String?, type: String?, sender: String?) {
        self.message = message
        self.timeStamp = timeStamp
        self.isIncoming = isIncoming
        self.type = MessageType(rawValue: type ?? "text")
        self.imageData = imageData
        self.sender = sender
    }
    
    func getTimeComponent() -> (day: String, time: String) {
        let day = self.timeStamp.getFormattedDay()
        let time = self.timeStamp.getFormattedForMessages()
        return (day: day, time: time)
    }
}
