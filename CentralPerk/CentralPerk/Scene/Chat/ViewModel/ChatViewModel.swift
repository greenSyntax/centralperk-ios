//
//  ChatViewModel.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 22/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation
import UIKit

struct MessageViewModel {
    var date: Date
    var day: String
    var messages: [ChatMessageModel]
    
    init(date: Date, day: String, messages: [ChatMessageModel]) {
        self.date = date
        self.day = day
        self.messages = messages
    }
}

class ChatViewModel: BaseViewModel {
 
    var hanldleUpdate: ((_ isSuccess: Bool) -> Void)?
    var handlerSuccess: ((_ messages: [ChatMessageModel]) -> Void)?
    var handlerDismiss: (() -> Void)?
    var handlerError: ((_ error: AppError) -> Void)?
    
    var friend: UserModel!
    var user: UserModel!
    
    var posts: [MessageViewModel] = []
    var messages: [ChatMessageModel] = []
    
    var loggedInUserId: String = {
        return PreferenceManager.shared.userId!
    }()
    
    lazy var imagePicker = ImagePickerWrapper()
    
    var chatId: String?
    
    init(friend: UserModel, user: UserModel) {
        self.friend = friend
        self.user = user
    }
    
    func getData() {
        guard let chatId = self.friend.chatId else {
            //TODO: For First Time
            self.handlerError?(.illFormattedData)
            return
        }
        self.handlerViewState?(true)
        let query = "conversations/\(chatId)"
        RealtimeDatabaseManager.shared.readWithListner(path: query) { (promise: Promise<NSDictionary>) in
            switch promise {
            case .failure(let error):
                self.handlerError?(error)
                self.handlerViewState?(false)
            case .success(let data):
                self.messages.removeAll()
                for (_, value) in data {
                    if let dict = value as? NSDictionary, let message = self.prepareChatMessages(dict) {
                        self.messages.append(message)
                    }
                }
                //self.registerForStatusListner()
                print("Messages Count: \(self.messages.count)")
                self.prepareMessagePosts(self.messages)
                self.handlerSuccess?(self.messages)
                self.handlerViewState?(false)
            }
        }
        
        self.handlerSuccess?([])
    }

    func sendMessages(text: String) {
        let payload = prepareChatMessagePayload(text)
        createConversationObject(payload)
        publishNotification(text: text, title: self.user.name ?? AppConfig.appName)
    }
    
    func sendImage(_ image: UIImage) {
        guard let imageData = image.inJPEG()?.base64EncodedString() else { return }
        let payload = prepareImagePayload(imageData)
        createConversationObject(payload)
        publishNotification(text: "Shared Image with you", title: self.user.name ?? AppConfig.appName)
    }
    
    func publishNotification(text: String, title: String) {
        guard let chatId = self.friend.chatId else { return }
        FirebaseMessagingManager().publish(topic: .chat(conversationId: chatId), title: title, text: text, success: {
            print("Successfully Sent Notification.")
        }) {
            print("Erro while sending Notification.")
        }
    }
    
    func createConversationObject(_ payload: [String: Any]) {
        
        if let chatId = friend.chatId, let messageId = RealtimeDatabaseManager.shared.getChildId() {
            let query = "conversations/\(chatId)/\(messageId)"
            RealtimeDatabaseManager.shared.update(child: query, data: payload) { (promise: Promise<Bool>) in
                switch promise {
                case .failure(let error):
                    self.handlerError?(error)
                case .success(let isSuccess):
                    print("Sent Status: \(isSuccess)")
                    //self.getData()
                    self.hanldleUpdate?(isSuccess)
                }
            }
        }
    }
    
    func pickImage(view: UIViewController) {
        self.imagePicker.show(view, delegate: self)
    }
    
//    func registerForStatusListner() {
//        if let uid = friend.uid {
//            let query = "users/\(uid)/status"
//            RealtimeDatabaseManager.shared.readWithListner(path: query) { (promise: Promise<String>) in
//                switch promise {
//                case .failure(let error):
//                    print(error)
//                case .success(let status):
//                    print(status)
//                }
//            }
//        }
//    }
    
    private func prepareChatMessagePayload(_ text: String) -> [String: Any] {
        var dict = [String: String]()
        dict["message"] = text
        dict["timestamp"] = Utility.getTimestamp()
        dict["senderId"] = loggedInUserId
        dict["sender"] = user.name
        dict["type"] = "text"
        
        return dict
    }
    
    private func prepareImagePayload(_ data: String) -> [String: Any]  {
        var dict = [String: String]()
        dict["imageData"] = data
        dict["timestamp"] = Utility.getTimestamp()
        dict["senderId"] = loggedInUserId
        dict["sender"] = user.name
        dict["type"] = "image"
        
        return dict
    }
    
    private func prepareChatMessages(_ dict: NSDictionary) -> ChatMessageModel? {
        if let dict = dict as? [String: Any],
        let userId = dict["senderId"] as? String,
        let timestamp = dict["timestamp"] as? String,
        let timestampInDouble = Double(timestamp) {
            let imageData = dict["imageData"] as? String
            let message = dict["message"] as? String
            let type = dict["type"] as? String //TODO: Required
            let sender = dict["sender"] as? String
            return ChatMessageModel(message: message, timeStamp: Utility.getDateFromTimestamp(timeStamp: timestampInDouble), isIncoming: self.loggedInUserId != userId, imageData: imageData, type: type, sender: sender)
        }
        return nil
    }
    
    func sortMessages(_ messages: [ChatMessageModel]) -> [ChatMessageModel] {
        let sortedMessages = messages.sorted { (first, second) -> Bool in
            return first.timeStamp.compare(second.timeStamp) == .orderedAscending
        }
        return sortedMessages
    }
}

extension ChatViewModel {
    
    func prepareMessagePosts(_ messages: [ChatMessageModel]) {
        var distinctDates = Set<String>()
        self.posts.removeAll()
        var timestamps = self.messages.map { (model) -> String in
            return model.getTimeComponent().day
        }
        timestamps.reverse()
        timestamps.forEach { (day) in distinctDates.insert(day) }
        
        distinctDates.forEach { (day) in
            var selectedDate = Date()
            var messageBucket: [ChatMessageModel] = []
            messages.forEach { (message) in
                if message.getTimeComponent().day == day {
                    selectedDate = message.timeStamp
                    messageBucket.append(message)
                }
            }
            if !messageBucket.isEmpty {
                let sortedMessages = messageBucket.sorted { (first, second) -> Bool in
                    return first.timeStamp.compare(second.timeStamp) == .orderedAscending
                }
                self.posts.append(MessageViewModel(date: selectedDate, day: day, messages: sortedMessages))
            }
        }
        
        // Sort
        self.posts = posts.sorted { (first, second) -> Bool in
            return first.date.compare(second.date) == .orderedAscending
        }
        
        
        print("Post's Date: \(self.posts.count)")
    }
    
    func removeFriend() {
        guard let friendUID = friend.uid, let userUID = user.uid else { return }
        self.handlerViewState?(true)
        UserService().removeFriend(friendUID: friendUID, userUID: userUID) { (promise: Promise<Bool>) in
            self.handlerViewState?(false)
            switch promise {
            case .failure(let error):
                self.handlerError?(error)
            case .success(let isDeleted):
                if isDeleted {
                    self.handlerDismiss?()
                } else {
                    self.hanldleUpdate?(false)
                }
            }
        }
    }
    
    func getPhotos() {
        
    }
    
    func getNumberOfSections() -> Int {
        return self.posts.count
    }
    
    func getNumberOfRows(section: Int) -> Int {
        return self.posts[section].messages.count
    }
    
    func getRowData(indexPath: IndexPath) -> ChatMessageModel {
        return self.posts[indexPath.section].messages[indexPath.row]
    }
    
    func getSectionData(section: Int) -> String {
        return self.posts[section].day
    }
    
}

extension ChatViewModel: ImagePickerWrapperDelegate {
    func didSelectedImage(_ image: UIImage?) {
        if let image = image {
            self.sendImage(image)
        }
    }
}
