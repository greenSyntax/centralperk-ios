//
//  GroupListViewController.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 19/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

class GroupListViewController: BaseViewController {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonMenu: UIButton!
    @IBOutlet weak var buttonAdd: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    weak var delegate: MenuBarDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initializeView()
    }
    
    func initializeView() {
        
        initializeTablView()
        setTableView()
        
        // Add
        buttonAdd.tintColor = UIColor.darkGray
        buttonAdd.titleLabel?.font = UIFont.fontAwesome(ofSize: 20, style: .solid)
        buttonAdd.setTitle(String.fontAwesomeIcon(name: .plus), for: .normal)
        
        // Menu
        buttonMenu.tintColor = UIColor.darkGray
        buttonMenu.titleLabel?.font = UIFont.fontAwesome(ofSize: 20, style: .solid)
        buttonMenu.setTitle(String.fontAwesomeIcon(name: .bars), for: .normal)
    }
    
    func initializeTablView() {
        tableView.separatorInset = .zero
    }
    
    func setTableView() {
        tableView.delegate = self
        tableView.dataSource = self
    }

    @IBAction func actionButtonMenu(_ sender: Any) {
        delegate?.onMenuAction()
    }
    
    @IBAction func actionButtonAdd(_ sender: Any) {
        //TODO: Add Invite and Create
    }
    
}

extension GroupListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
}
