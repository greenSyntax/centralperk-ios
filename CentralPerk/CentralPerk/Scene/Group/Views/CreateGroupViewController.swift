//
//  CreateGroup.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 26/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

class CreateGroupViewController: BaseViewController {
    
    @IBOutlet weak var buttonCreate: UIButton!
    @IBOutlet weak var textFieldGroupName: UITextField!
    @IBOutlet weak var imageGroupIcon: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        intializeView()
    }
        
    private func intializeView() {
        
    }
    
    @IBAction func buttonCreateAndInviteAction(_ sender: Any) {
        
    }
    
}
