//
//  GroupViewController.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 11/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

class GroupViewController: BaseViewController {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var textFieldMessage: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonSend: UIButton!
    @IBOutlet weak var buttonMenu: UIButton!
    @IBOutlet weak var buttonAttachement: UIButton!
    @IBOutlet weak var buttonOptions: UIButton!
    @IBOutlet weak var viewSendContainer: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    var viewModel: GroupViewModel!
    
    weak var delegate: MenuBarDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeView()
        getChatData()
    }
    
    func initializeView() {
        bindViewModel()
        initializeTableView()
        setIcons()
        moreButton(buttonOptions)
        defaultTexts()
        footerSendMessageView()
        registerOutsideTap()
        registerCells()
        
        labelTitle.text = viewModel.getTitle()
    }
    
    func defaultTexts() {
        self.textFieldMessage.placeholder = AppText.chatTextFieldPlaceholder
    }
    
    func initializeTableView() {
        self.tableView.separatorStyle = .none
    }
    
    func footerSendMessageView() {
        viewSendContainer.layer.cornerRadius = viewSendContainer.layer.bounds.height / 2
        viewSendContainer.layer.borderColor = UIColor.lightGray.cgColor
        viewSendContainer.layer.borderWidth = 1.0
    }
    
    func setIcons() {
        // Menu
        buttonMenu.tintColor = UIColor.darkGray
        buttonMenu.titleLabel?.font = UIFont.fontAwesome(ofSize: 20, style: .solid)
        buttonMenu.setTitle(String.fontAwesomeIcon(name: .bars), for: .normal)
        
        // Attachemnt
        buttonAttachement.tintColor = UIColor.darkGray
        buttonAttachement.titleLabel?.font = UIFont.fontAwesome(ofSize: 20, style: .solid)
        buttonAttachement.setTitle(String.fontAwesomeIcon(name: .paperclip), for: .normal)
        
        //Send
        buttonSend.tintColor = UIColor.black
        buttonSend.titleLabel?.font = UIFont.fontAwesome(ofSize: 24, style: .solid)
        buttonSend.setTitle(String.fontAwesomeIcon(name: .arrowRight), for: .normal)
    }
    
    private func registerCells() {
        tableView.register(UINib(nibName: String(describing: MessageCell.self), bundle: nil), forCellReuseIdentifier: String(describing: MessageCell.self))
        tableView.register(UINib(nibName: String(describing: ImageMessageCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ImageMessageCell.self))
        tableView.register(UINib(nibName: String(describing: DateView.self), bundle: nil), forHeaderFooterViewReuseIdentifier: String(describing: DateView.self))
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func getChatData() {
        viewModel.getPublicChatMessages()
    }
    
    @IBAction func actionButtonMenu(_ sender: Any) {
        delegate?.onMenuAction()
    }
    
    @IBAction func actionButtonAttachment(_ sender: Any) {
        viewModel.pickImage(view: self)
    }
    
    @IBAction func actionButtonOptions(_ sender: Any) {
        viewModel.showOptions(view: self)
    }
    
    @IBAction func actionButtonSend(_ sender: Any) {
        guard let text = textFieldMessage.text, !text.isEmpty else { return }
        viewModel.send(text: text)
        dismissResponder()
    }
    
    func bindViewModel() {
        
        viewModel.handlerSuccess = { (_ messages: [ChatMessageModel]) in
            
            if !messages.isEmpty {
                self.tableView.reloadData()
            }
            
            if let _ = self.tableView.indexPathsForVisibleRows?.last {
                self.tableView.layoutIfNeeded()
                self.tableView.scrollToBottom(animated: true)
            }
        }
        
        self.viewModel.handlerViewState = { [weak self] (_ isLoading: Bool) in
            guard let view = self?.tableView else { return }
            self?.state = isLoading ? ViewState.area(view: view) : ViewState.done
        }
            
        self.viewModel.handlerError = { [weak self] (_ error: AppError) in
            //self?.state = .error(error: error)
            print("Error: \(error.text)")
        }

        self.viewModel.handlerUpdate = { [weak self] (_ isSuccess: Bool) in
            if isSuccess {
                self?.textFieldMessage.text = ""
            }
        }
        
//        self.viewModel.handlerDismiss = { [weak self] () in
//            NotificationCenter.default.post(name: Notification.Name("UpdateFriendsList"), object: nil)
//            self?.navigationController?.popViewController(animated: true)
//        }
    }
    
}

extension GroupViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.getNumberOfSections()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumberOfRows(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //print("Section: \(indexPath.section) Row: \(indexPath.row)")
        let vm = viewModel.getRowData(indexPath: indexPath)
        if let type = vm.type {
            switch type {
            case .image:
                if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ImageMessageCell.self)) as? ImageMessageCell {
                    cell.delegate = self
                    cell.configure(model: vm)
                    return cell
                }
            case .text:
                if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MessageCell.self)) as? MessageCell {
                    cell.configure(model: vm)
                    return cell
                }
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UINib(nibName: String(describing: DateView.self), bundle: nil).instantiate(withOwner: self, options: [:]).first as? DateView
        view?.configure(day: viewModel.getSectionData(section: section))
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}

extension GroupViewController {
    override func onKeyboardShown(_ keyboardSize: CGRect) {
        let value = keyboardSize.size.height
        let extra = UIDevice.hasNotch ? 70 : 0
        bottomConstraint.constant = (value + CGFloat(extra))
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func onKeyboardHide() {
        bottomConstraint.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}

extension GroupViewController: ImageMessageCellDelegate {
    func onClickImageView(model: ChatMessageModel) {
        if let vc = PreviewViewController.loadViewController() as? PreviewViewController {
            guard let base64 = model.imageData, let imageData = Data(base64Encoded: base64) else { return }
            vc.image = UIImage(data: imageData)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
}
