//
//  GroupViewModel.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 15/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

class GroupViewModel: BaseViewModel {
    
    lazy var imagePicker = ImagePickerWrapper()
    

    var handlerSuccess: ((_ data: [ChatMessageModel]) -> Void)?
    var handlerUpdate: ((_ hasUpdated: Bool) -> Void)?
    var handlerError: ((_ error: AppError) -> Void)?
    
    var user: UserModel!
    
    var posts: [MessageViewModel] = []
    var messages: [ChatMessageModel] = []
    
    init(user: UserModel) {
        self.user = user
    }
    
    func send(text: String) {
        //  group collection => [groupId] => [messageid] => [message, senderId, timestamp, sender, type]
        let payload = prepareChatMessagePayload(text)
        publishMessage(data: payload)
    }
    
    func send(image: UIImage) {
        guard let imageData = image.inJPEG()?.base64EncodedString() else { return }
        let payload = prepareImagePayload(imageData)
        publishMessage(data: payload)
    }
    
    func publishMessage(data: [String: Any]) {
        guard let messageId = RealtimeDatabaseManager.shared.getChildId() else { return }
        let query = "groups/public/\(messageId)"
        RealtimeDatabaseManager.shared.update(child: query, data: data) { (promise: Promise<Bool>) in
            switch promise {
            case .failure(let error):
                self.handlerError?(error)
            case .success(let isUpdated):
                self.handlerUpdate?(isUpdated)
                
            }
        }
    }
    
    //TODO: Make a Base Class for this
    private func prepareChatMessagePayload(_ text: String) -> [String: Any] {
        guard let userId = user.uid else { return [:] }
        var dict = [String: String]()
        dict["message"] = text
        dict["timestamp"] = Utility.getTimestamp()
        dict["senderId"] = userId
        dict["sender"] = user.name
        dict["type"] = "text"
        
        return dict
    }
    
    private func prepareImagePayload(_ data: String) -> [String: Any]  {
        guard let userId = user.uid else { return [:] }
        var dict = [String: String]()
        dict["imageData"] = data
        dict["timestamp"] = Utility.getTimestamp()
        dict["senderId"] = userId
        dict["sender"] = user.name
        dict["type"] = "image"
        
        return dict
    }
    
}

// MARK: - get Messages for public room
extension GroupViewModel {
    func getPublicChatMessages() {
        self.handlerViewState?(true)
        let query = "groups/public"
        RealtimeDatabaseManager.shared.readWithListner(path: query) { (promise: Promise<[String: Any]>) in
            switch promise {
            case .failure(let error):
                self.handlerError?(error)
            case .success(let dict):
                self.messages.removeAll()
                for (_, value) in dict {
                    if let dict = value as? NSDictionary, let message = self.prepareChatMessages(dict) {
                        self.messages.append(message)
                    }
                    
                    print("Messages Count: \(self.messages.count)")
                    self.prepareMessagePosts(self.messages)
                    self.handlerSuccess?(self.messages)
                    self.handlerViewState?(false)
                }
            }
        }
    }
    
    //TODO: Redundant
    func prepareChatMessages(_ dict: NSDictionary) -> ChatMessageModel? {
        guard let uid = self.user.uid else { return nil }
        if let dict = dict as? [String: Any],
        let userId = dict["senderId"] as? String,
        let timestamp = dict["timestamp"] as? String,
        let timestampInDouble = Double(timestamp) {
            let imageData = dict["imageData"] as? String
            let message = dict["message"] as? String
            let type = dict["type"] as? String //TODO: Required
            let sender = dict["sender"] as? String
            return ChatMessageModel(message: message, timeStamp: Utility.getDateFromTimestamp(timeStamp: timestampInDouble), isIncoming: uid != userId, imageData: imageData, type: type, sender: sender)
        }
        return nil
    }
    
    func sendImage(_ image: UIImage) {
        guard let imageData = image.inJPEG()?.base64EncodedString() else { return }
        let payload = prepareImagePayload(imageData)
        createConversationObject(payload)
        publishNotification(text: "Shared Image with you", title: self.user.name ?? AppConfig.appName)
    }
    
    func publishNotification(text: String, title: String) {
        
//        FirebaseMessagingManager().publish(topic: .chat(conversationId: chatId), title: title, text: text, success: {
//            print("Successfully Sent Notification.")
//        }) {
//            print("Erro while sending Notification.")
//        }
    }
    
    func createConversationObject(_ payload: [String: Any]) {
        
        if let uid = user.uid, let messageId = RealtimeDatabaseManager.shared.getChildId() {
            let query = "groups/public/\(messageId)"
            RealtimeDatabaseManager.shared.update(child: query, data: payload) { (promise: Promise<Bool>) in
                switch promise {
                case .failure(let error):
                    self.handlerError?(error)
                case .success(let isSuccess):
                    print("Sent Status: \(isSuccess)")
                    self.handlerUpdate?(isSuccess)
                }
            }
        }
    }
    
}

extension GroupViewModel {
    
    func prepareMessagePosts(_ messages: [ChatMessageModel]) {
          var distinctDates = Set<String>()
          self.posts.removeAll()
          var timestamps = self.messages.map { (model) -> String in
              return model.getTimeComponent().day
          }
          timestamps.reverse()
          timestamps.forEach { (day) in distinctDates.insert(day) }
          
          distinctDates.forEach { (day) in
              var selectedDate = Date()
              var messageBucket: [ChatMessageModel] = []
              messages.forEach { (message) in
                  if message.getTimeComponent().day == day {
                      selectedDate = message.timeStamp
                      messageBucket.append(message)
                  }
              }
              if !messageBucket.isEmpty {
                  let sortedMessages = messageBucket.sorted { (first, second) -> Bool in
                      return first.timeStamp.compare(second.timeStamp) == .orderedAscending
                  }
                  self.posts.append(MessageViewModel(date: selectedDate, day: day, messages: sortedMessages))
              }
          }
          
          // Sort
          self.posts = posts.sorted { (first, second) -> Bool in
              return first.date.compare(second.date) == .orderedAscending
          }
          
          
          print("Post's Date: \(self.posts.count)")
      }
    
    func getTitle() -> String {
        //TODO:
        return "Public Room"
    }
    
    func pickImage(view: UIViewController) {
        self.imagePicker.show(view, delegate: self)
    }
    
    func showOptions(view: UIViewController) {
        
    }
    
    func getNumberOfSections() -> Int {
        return self.posts.count
    }
    
    func getNumberOfRows(section: Int) -> Int {
        return self.posts[section].messages.count
    }
    
    func getRowData(indexPath: IndexPath) -> ChatMessageModel {
        return self.posts[indexPath.section].messages[indexPath.row]
    }
    
    func getSectionData(section: Int) -> String {
        return self.posts[section].day
    }
    
}

extension GroupViewModel: ImagePickerWrapperDelegate {
    func didSelectedImage(_ image: UIImage?) {
        if let image = image {
            self.sendImage(image)
        }
    }
}
