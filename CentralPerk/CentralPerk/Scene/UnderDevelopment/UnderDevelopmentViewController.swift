//
//  UnderDevelopmentViewController.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 14/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

class UnderDevelopmentViewController: BaseViewController {

    @IBOutlet weak var buttonMenu: UIButton!
    @IBOutlet weak var imageCenter: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    
    weak var delegate: MenuBarDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setMenu()
    }
    
    func setMenu() {
        buttonMenu.tintColor = UIColor.darkGray
        buttonMenu.titleLabel?.font = UIFont.fontAwesome(ofSize: 20, style: .solid)
        buttonMenu.setTitle(String.fontAwesomeIcon(name: .bars), for: .normal)
    }
    
    @IBAction func actionButtonMenu(_ sender: Any) {
        delegate?.onMenuAction()
    }

}
