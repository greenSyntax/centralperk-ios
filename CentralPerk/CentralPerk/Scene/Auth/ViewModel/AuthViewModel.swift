//
//  AuthViewModel.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 20/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation
import UIKit

class BaseViewModel {
    
    init() {}
    
    var handlerViewState: ((_ isLoading: Bool) -> Void)?
}

class AuthViewModel: BaseViewModel {
    
    var handlerSuccess: ((_ user: LoggedInUser) -> Void)?
    var handlerError: ((_ error: AppError) -> Void)?
    var handleProfilePicture: ((_ image: UIImage) -> Void)?
    var handleUpdateStatus: ((_ text: String) -> Void)?
    
    lazy var imagePicker = ImagePickerWrapper()
    var loggedInUser: LoggedInUser? //TODO: Remove this type
    var userProfile: UserModel?
    
    func createUser(email: String, password: String, mobile: String) {
        let model = LoginUserModel(email: email, password: password)
        self.handlerViewState?(true)
        AuthenticationManager().signup(userModel: model) { (promise: Promise<LoggedInUser>) in
            self.handlerViewState?(false)
            switch promise {
            case .success(let loggedInUser):
                //TODO: Fix This Later
                self.loggedInUser = loggedInUser
                var user = loggedInUser
                user.phoneNumber = mobile
                self.handlerSuccess?(user)
            case .failure(let error):
                self.handlerError?(error)
            }
        }
    }
    
    func loginUser(email: String, password: String) {
        let model = LoginUserModel(email: email, password: password)
        self.handlerViewState?(true)
        AuthenticationManager().signin(userModel: model) { (promise: Promise<LoggedInUser>) in
            self.handlerViewState?(false)
            switch promise {
            case .success(let loggedInUser):
                self.loggedInUser = loggedInUser
                self.handlerSuccess?(loggedInUser)
            case .failure(let error):
                self.handlerError?(error)
            }
        }
    }
    
    func resetpassword(email: String) {
        self.handlerViewState?(true)
        AuthenticationManager().resetPassword(email: email) { (promise: Promise<String>) in
            self.handlerViewState?(false)
            switch promise {
            case .failure(let error):
                self.handlerError?(error)
            case .success(let status):
                self.handleUpdateStatus?(status)
            }
        }
    }
    
    
    func createProfile(user: LoggedInUser) {
        let query = "users/\(user.userId)"
        self.handlerViewState?(true)
        RealtimeDatabaseManager.shared.write(child: query, data: user.toDict()) { (promise: Promise<Bool>) in
            self.handlerViewState?(false)
            switch promise {
            case .failure(let error):
                self.handlerError?(error)
            case .success(_):
                self.userProfile = UserModel(dict: user.toDict() as NSDictionary)
                self.handlerSuccess?(user)
            }
        }
    }
 
    func checkUserProfileCreated(onCompletion: @escaping(_ hasCreated: Bool) -> Void) {
        if let user = loggedInUser {
            let query = "users/\(user.userId)"
            self.handlerViewState?(true)
            RealtimeDatabaseManager.shared.read(path: query) { (promise: Promise<NSDictionary>) in
                self.handlerViewState?(false)
                switch promise {
                case .failure(let error):
                    print("Erorr: \(error.text)")
                    onCompletion(false)
                case .success(let data):
                    self.userProfile = UserModel(dict: data)
                    onCompletion(true)
                }
            }
        }
    }
    
    func updateProfileImage(_ view: UIViewController) {
        imagePicker.show(view, delegate: self)
    }
}

extension AuthViewModel: ImagePickerWrapperDelegate {
    
    private func getUserId() -> String? {
        if let uid = self.userProfile?.uid {
            return uid
        }
        if let uid = self.loggedInUser?.userId {
            return uid
        }
        return nil
    }
    
    func didSelectedImage(_ image: UIImage?) {
        guard let image = image else { return }
        guard let uid = getUserId() else { return }
        self.handleProfilePicture?(image)
        
        StorageManager().upload(image: image) { (url, error) in
            RealtimeDatabaseManager.shared.update(child: "users/\(uid)", data: ["profilePictureURL": url?.absoluteString ?? ""]) { (promise: Promise<Bool>) in
                switch promise {
                case .failure(let error):
                    self.handlerError?(error)
                case .success(let isUpdated):
                    self.handleUpdateStatus?(isUpdated ? "Successfully Added Your Profile Picture" : "Please try again")
                }
            }
        }

    }
}

