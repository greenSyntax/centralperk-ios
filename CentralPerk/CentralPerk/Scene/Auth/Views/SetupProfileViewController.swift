//
//  SetupProfileViewController.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 20/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class SetupProfileViewController: BaseViewController {

    @IBOutlet weak var buttonCreateProfile: UIButton!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var textFieldNickName: JVFloatLabeledTextField!
    @IBOutlet weak var textFieldMobile: JVFloatLabeledTextField!
    @IBOutlet weak var textFieldEmail: JVFloatLabeledTextField!
    @IBOutlet weak var buttonEditAction: UIButton!
    
    var viewModel: AuthViewModel!
    var loggedInUser: LoggedInUser!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initializeView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        bindViewModel()
    }
    
    func initializeView() {
        self.imageUser.layer.cornerRadius = self.imageUser.bounds.height / 2
        self.imageUser.layer.borderColor = UIColor.lightGray.cgColor
        self.imageUser.layer.borderWidth = 1.0
        self.textFieldEmail.isEnabled = false
        self.registerOutsideTap()
        self.setBackButton(self.buttonBack)
        self.populateView(user: self.loggedInUser)
        self.setEditButton()
    }
    
    func setEditButton() {
        buttonEditAction.layer.cornerRadius = buttonBack.bounds.height / 2
        buttonEditAction.backgroundColor = UIColor.darkGray
        buttonEditAction.layer.borderColor = UIColor.white.cgColor
        buttonEditAction.layer.borderWidth = 1.0
        buttonEditAction.tintColor = UIColor.white
        buttonEditAction.titleLabel?.font = UIFont.fontAwesome(ofSize: 20, style: .solid)
        buttonEditAction.setTitle(String.fontAwesomeIcon(name: .pencilAlt), for: .normal)
    }
    
    func populateView(user: LoggedInUser) {
        self.textFieldEmail.text = user.email
        self.textFieldMobile.text = user.phoneNumber
        self.textFieldNickName.text = user.name
    }
    
    func bindViewModel() {
        
        self.viewModel.handlerSuccess = { [weak self] (_ user: LoggedInUser) in
            self?.showToast(message: AppText.successfullyProfile)
            self?.routeToHome()
        }
        
        self.viewModel.handlerViewState = { [weak self] (_ isLoading: Bool) in
            self?.state = isLoading ? ViewState.loading : ViewState.done
        }
        
        self.viewModel.handlerError = { [weak self] (_ error: AppError) in
            self?.state = .error(error: error)
        }
        
        self.viewModel.handleUpdateStatus = { [weak self] (_ text: String) in
            self?.showToast(message: text)
        }
        
        self.viewModel.handleProfilePicture = { [weak self] (_ image: UIImage) in
            self?.imageUser.image = image
        }
    }
    
    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionButtonEdit(_ sender: Any) {
        viewModel.updateProfileImage(self)
    }
    
    @IBAction func buttonCreateProfileAction(_ sender: Any) {
        guard let nickName = self.textFieldNickName.text, !nickName.isEmpty else {
            self.showToast(message: AppException.nameRequired)
            return
        }
        
        guard let email = self.textFieldEmail.text, !email.isEmpty else {
            self.showToast(message: AppException.emailRequired)
            return
        }
        
        guard let mobile = self.textFieldMobile.text, !mobile.isEmpty else {
            self.showToast(message: AppException.mobileRequired)
            return
        }
        
        if var user = self.viewModel.loggedInUser {
            user.name = nickName
            user.phoneNumber = mobile
            viewModel.createProfile(user: user)
        }
    }

}

extension SetupProfileViewController {
    
    func routeToHome() {
        if let user = self.viewModel.userProfile,
            let uid = user.uid {
            PreferenceManager.shared.userId = uid
            UIApplication.shared.keyWindow?.rootViewController = ContainerViewController(user: user)
        }
    }
    
}
