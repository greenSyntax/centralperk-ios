//
//  SignupViewController.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 20/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class SignupViewController: BaseViewController {

    @IBOutlet weak var labelHeadText: UILabel!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var textFieldEmail: JVFloatLabeledTextField!
    @IBOutlet weak var textFieldPassword: JVFloatLabeledTextField!
    @IBOutlet weak var textFieldMobile: JVFloatLabeledTextField!
    @IBOutlet weak var buttonRegister: UIButton!
    
    var viewModel: AuthViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        bindViewModel()
    }

    func initializeView() {
        self.labelHeadText.text = AppText.signUpText
        self.registerOutsideTap()
        self.setBackButton(self.buttonBack)
    }
    
    func bindViewModel() {
        
        self.viewModel.handlerSuccess = { [weak self] (_ user: LoggedInUser) in
            self?.showToast(message: AppText.successfullySignUp)
            self?.routeToSetupProfile(user)
        }
        
        self.viewModel.handlerViewState = { [weak self] (_ isLoading: Bool) in
            self?.state = isLoading ? ViewState.loading : ViewState.done
        }
        
        self.viewModel.handlerError = { [weak self] (_ error: AppError) in
            self?.state = .error(error: error)
        }
    }
    
    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonRegisterAction(_ sender: Any) {
        guard let email = self.textFieldEmail.text, !email.isEmpty else {
            self.showToast(message: AppException.emailRequired)
            return
        }
        guard let password = self.textFieldPassword.text, !password.isEmpty else {
            self.showToast(message: AppException.passwordRequired)
            return
        }
        guard let mobile = self.textFieldMobile.text, !mobile.isEmpty else {
            self.showToast(message: AppException.mobileRequired)
            return
        }
        
        self.viewModel.createUser(email: email, password: password, mobile: mobile)
        dismissResponder()
    }
    
}

extension SignupViewController {
    
    func routeToSetupProfile(_ user: LoggedInUser) {
        if let vc = SetupProfileViewController.loadViewController() as? SetupProfileViewController {
            vc.viewModel = self.viewModel
            vc.loggedInUser = user
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
