//
//  ResetPasswordViewController.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 19/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class ResetPasswordViewController: BaseViewController {
    
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var textFieldEmail: JVFloatLabeledTextField!
    @IBOutlet weak var buttonResetPassword: UIButton!
    
    var viewModel: AuthViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bind()
        initializeView()
    }
    
    func initializeView() {
        self.setBackButton(self.buttonBack)
    }
    
    @IBAction func actionBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionResetPassword(_ sender: Any) {
        guard let email = self.textFieldEmail.text, !email.isEmpty  else { return }
        viewModel.resetpassword(email: email)
        dismissResponder()
    }
    
    func bind() {
        self.viewModel.handlerSuccess = { [weak self] (_ user: LoggedInUser) in
            //TODO:
        }
        
        self.viewModel.handlerViewState = { [weak self] (_ isLoading: Bool) in
            self?.state = isLoading ? ViewState.loading : ViewState.done
        }
        
        self.viewModel.handlerError = { [weak self] (_ error: AppError) in
            self?.state = .error(error: error)
        }
        
        self.viewModel.handleUpdateStatus = { [weak self] (_ text: String) in
            self?.showToast(message: AppText.successfullResetPassword)
        }
    }

}
