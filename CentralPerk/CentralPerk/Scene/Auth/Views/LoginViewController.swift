//
//  LoginViewController.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 20/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField
import FontAwesome_swift

class LoginViewController: BaseViewController {

    @IBOutlet weak var buttonForgotPassword: UIButton!
    @IBOutlet weak var labelHeadTitle: UILabel!
    @IBOutlet weak var buttonRegister: UIButton!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var buttonLogin: UIButton!
    @IBOutlet weak var textFieldEmail: JVFloatLabeledTextField!
    @IBOutlet weak var textFieldPassword: JVFloatLabeledTextField!
    
    var viewModel: AuthViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        bindViewModel()
    }
    
    func initializeView() {
        self.labelHeadTitle.text = AppText.loginHeadText
        self.registerOutsideTap()
        self.setBackButton(self.buttonBack)
    }
    
    func bindViewModel() {
        
        self.viewModel.handlerSuccess = { [weak self] (_ user: LoggedInUser) in
            self?.showToast(message: AppText.successfullyLogin)
            self?.routeToScreen()
        }
        
        self.viewModel.handlerViewState = { [weak self] (_ isLoading: Bool) in
            self?.state = isLoading ? ViewState.loading : ViewState.done
        }
        
        self.viewModel.handlerError = { [weak self] (_ error: AppError) in
            self?.state = .error(error: error)
        }
    }
    
    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonLoginAction(_ sender: Any) {
        guard let email = self.textFieldEmail.text, !email.isEmpty else {
            self.showToast(message: AppException.emailRequired)
            return
        }
        
        guard let password = self.textFieldPassword.text, !password.isEmpty else {
            self.showToast(message: AppException.passwordRequired)
            return
        }
        
        self.viewModel.loginUser(email: email, password: password)
        dismissResponder()
    }
    
    @IBAction func buttonRegiseterAction(_ sender: Any) {
        routeToSignUp()
    }
    
    @IBAction func actionForgotPassword(_ sender: Any) {
        routeToResetPassword()
    }
    
}

extension LoginViewController {
    
    func routeToScreen() {
        viewModel.checkUserProfileCreated(onCompletion: { [weak self] (hasCreated) in
            if hasCreated {
                if let user = self?.viewModel.userProfile,
                    let uid = user.uid {
                    PreferenceManager.shared.userId = uid
                    UIApplication.shared.keyWindow?.rootViewController = ContainerViewController(user: user)
                }
            } else {
                if let vc = SetupProfileViewController.loadViewController() as? SetupProfileViewController,
                    let user = self?.viewModel.loggedInUser {
                    vc.viewModel = self?.viewModel
                    vc.loggedInUser = user
                    UIApplication.shared.keyWindow?.rootViewController = UINavigationController(rootViewController: vc)
                }
            }
        })
    }
    
    func routeToSignUp() {
        if let vc = SignupViewController.loadViewController() as? SignupViewController {
            vc.viewModel = self.viewModel
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func routeToResetPassword() {
        if let vc = ResetPasswordViewController.loadViewController() as? ResetPasswordViewController {
            vc.viewModel = self.viewModel
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
