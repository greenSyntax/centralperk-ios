//
//  NavigationMenuCell.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 22/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

class NavigationMenuCell: UITableViewCell {

    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
    lazy var selectionView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.white
        self.selectedBackgroundView = selectionView
    }
    
    func configure(menu: MenuItem) {
        imageCell.image = menu.asset.image
        labelTitle.text = menu.asset.title
    }
}
