//
//  NavigationMenuViewController.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 22/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

enum MenuItem: CaseIterable {
    case friends
    case publicRoom
    case privateRoom
    case about
    case settings
    
    var asset: (title: String, image: UIImage) {
        switch self {
        case .friends:
            return (title: "Your Friends", image: #imageLiteral(resourceName: "img_friends"))
        case .publicRoom:
            return (title: "Public Room", image: #imageLiteral(resourceName: "img_public"))
        case .privateRoom:
            return (title: "Personal Groups", image: #imageLiteral(resourceName: "img_private"))
        case .about:
            return (title: "About App", image: #imageLiteral(resourceName: "img_about"))
        case .settings:
            return (title: "Prefrences", image: #imageLiteral(resourceName: "img_settings"))
        }
    }
}

class NavigationMenuViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var buttonEdit: UIButton!
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var labelAppSemantic: UILabel!
    
    weak var user: UserModel!
    weak var delegate: MenuViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initializeView()
    }
    
    func setup() {
        self.labelTitle.text = user.name
        self.labelDescription.text = user.email
        self.imageProfile.layer.cornerRadius = self.imageProfile.bounds.height / 2
        self.imageProfile.layer.borderWidth = 1.0
        self.imageProfile.layer.borderColor = UIColor.lightGray.cgColor
        self.tableView.backgroundColor = UIColor.white
        
        if let path = user.profilePictureURL {
            RemoteImage.get(path, imageView: self.imageProfile) { (isLoader) in
                print("Successfull for \(path)")
            }
        }
    }
    
    private func initializeView() {
        setup()
        registerCell()
        initializeTableView()
        setEditButton()
        setGestureProfileHeader()
    }
    
    private func registerCell() {
        tableView.register(UINib(nibName: String(describing: NavigationMenuCell.self), bundle: nil), forCellReuseIdentifier: String(describing: NavigationMenuCell.self))
    }
    
    private func setEditButton() {
        buttonEdit.tintColor = UIColor.darkGray
        buttonEdit.titleLabel?.font = UIFont.fontAwesome(ofSize: 12, style: .solid)
        buttonEdit.setTitle(String.fontAwesomeIcon(name: .pen), for: .normal)
    }
    
    private func setGestureProfileHeader() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnProfile))
        self.viewNavigation.addGestureRecognizer(tapGesture)
    }
    
    private func initializeTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        //self.tableView.separatorInset = .zero
        self.tableView.separatorStyle = .none
        self.tableView.tableFooterView = UIView()
        self.setSemantic()
    }
    
    func setSemantic() {
        self.labelAppSemantic.text = "\(AppText.appNameFormatted) v\(Utility.semantic().version)(\(Utility.semantic().build))"
    }
    
    @objc func tappedOnProfile() {
        if let vc = ProfileViewController.loadViewController() as? ProfileViewController {
            vc.viewModel = ProfileViewModel(user: user, isEditable: true)
            self.present(vc, animated: true, completion: nil)
            delegate?.dismiss()
        }
    }
    
    @IBAction func actionButtonEdit(_ sender: Any) {
        tappedOnProfile()
    }
    
}

extension NavigationMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MenuItem.allCases.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NavigationMenuCell.self)) as! NavigationMenuCell
        cell.configure(menu: MenuItem.allCases[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        delegate?.onMenuItemClicked(MenuItem.allCases[indexPath.row])
    }
    
}
