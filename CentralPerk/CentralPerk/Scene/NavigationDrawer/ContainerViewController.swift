//
//  ContainerViewController.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 10/07/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

protocol MenuBarDelegate: class {
    func onMenuAction()
}

protocol MenuViewControllerDelegate: class {
    func onMenuItemClicked(_ item: MenuItem)
    func dismiss()
}

class ContainerViewController: UIViewController {
    
    var menuController: NavigationMenuViewController!
    var centerController: UIViewController!
    var isExpanded: Bool = false
    
    var user: UserModel!
    
    init(user: UserModel) {
        self.user = user
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    lazy var grayView: UIView = {
        let view = UIView()
        view.tag = 1001
        view.frame = UIScreen.main.bounds
        view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    func configure() {
        let listingController = ListingViewController.loadViewController() as! ListingViewController
        listingController.delegate = self
        listingController.viewModel = ListingViewModel(user: user)
        centerController = UINavigationController(rootViewController: listingController)
        view.addSubview(centerController.view)
        
        addChild(centerController)
        centerController.didMove(toParent: self)
    }
    
    func navigateToViewController(_ vc: UIViewController) {
        showMenu(shouldExpand: false)
        if let navigationController = centerController as? UINavigationController {
            navigationController.setViewControllers([vc], animated: true)
            view.addSubview(navigationController.view)
            addChild(navigationController)
            navigationController.didMove(toParent: self)
        }
    }
    
    func configureMenuController() {
        if menuController == nil {
            if let menuController = NavigationMenuViewController.loadViewController() as? NavigationMenuViewController {
                menuController.delegate = self
                menuController.user = user
                view.insertSubview(menuController.view, at: 0)
                addChild(menuController)
                menuController.didMove(toParent: self)
                
                grayView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedOnGrayArea)))
            }
        }
    }
    
    @objc func tappedOnGrayArea() {
        showMenu(shouldExpand: false)
    }
    
    func showMenu(shouldExpand: Bool) {
        if shouldExpand {
            self.centerController.view.addSubview(self.grayView)
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.centerController.view.frame.origin.x = self.centerController.view.frame.width - 100
            }, completion: nil)
        } else {
            self.centerController.view.subviews.forEach { (view) in
                if view.tag == 1001 { view.removeFromSuperview() }
            }
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.centerController.view.frame.origin.x = 0
            }, completion: nil)
        }
    }
}


extension ContainerViewController: MenuBarDelegate {
    func onMenuAction() {
        if !isExpanded {
            configureMenuController()
        }
        
        isExpanded = !isExpanded
        showMenu(shouldExpand: isExpanded)
    }
    
    func dismiss() {
        showMenu(shouldExpand: false)
    }
}

extension ContainerViewController: MenuViewControllerDelegate {
    func onMenuItemClicked(_ item: MenuItem) {
        switch item {
        case .friends:
            let vc = ListingViewController.loadViewController() as! ListingViewController
            vc.delegate = self
            vc.viewModel = ListingViewModel(user: user)
            navigateToViewController(vc)
        case .privateRoom:
            let vc = GroupListViewController.loadViewController() as! GroupListViewController
            vc.delegate = self
            navigateToViewController(vc)
        case .publicRoom:
            let vc = GroupViewController.loadViewController() as! GroupViewController
            vc.viewModel = GroupViewModel(user: user)
            vc.delegate = self
            navigateToViewController(vc)
        case .about:
            let vc = AboutViewController.loadViewController() as! AboutViewController
            vc.delegate = self
            navigateToViewController(vc)
        case .settings:
            let vc = SettingsViewController.loadViewController() as! SettingsViewController
            vc.viewModel = SettingsViewModel(user: self.user)
            vc.delegate = self
            navigateToViewController(vc)
        }
    }
}
