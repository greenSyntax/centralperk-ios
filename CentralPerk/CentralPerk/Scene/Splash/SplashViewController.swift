//
//  SplashViewController.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 21/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import UIKit

class SplashViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        initialize()
    }

    func initialize() {
        self.view.backgroundColor = UIColor.white
        getUserData()
        Loader().addLoader(self.view)
    }
    
    func setup() {
        FirebaseMessagingManager().register()
    }
    
    func routeToHome(user: UserModel) {
        setup()
        UIApplication.shared.keyWindow?.rootViewController = ContainerViewController(user: user)
    }
    
    func routeToLandingScreen() {
        let vc = LandingViewController.loadViewController()
        let navigationController = UINavigationController(rootViewController: vc)
        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }
    
    func getUserData() {
        guard AlamofireManager().isInternetAvailable() else {
            self.showToast(message: "Please check your Internet Connectivity.")
            return
        }
        if let uid = PreferenceManager.shared.userId {
            let query = "users/\(uid)"
            RealtimeDatabaseManager.shared.read(path: query) { (promise: Promise<NSDictionary>) in
                switch promise {
                case .failure(let error):
                    self.showToast(message: error.text)
                    self.routeToLandingScreen()
                case .success(let dict):
                    if let user = UserModel(dict: dict) {
                        self.routeToHome(user: user)
                    } else {
                        
                        self.routeToLandingScreen()
                    }
                }
            }
        } else {
            //TODO:
            self.showToast(message: "You need to re-install your app.")
        }
    }
    
}
