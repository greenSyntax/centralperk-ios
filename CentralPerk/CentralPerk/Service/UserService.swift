//
//  UserService.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 27/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation

enum UserStatus: String {
    case online
    case offline
    case lastSeen
}

class UserService {
    
    func updateStatus(status: UserStatus) {
        guard let uid = PreferenceManager.shared.userId else { return }
        RealtimeDatabaseManager.shared.update(child: "users/\(uid)", data: ["status": status.rawValue, "lastSeen": Utility.getTimestamp()]) { (promise: Promise<Bool>) in
            switch promise {
            case .failure(let error):
                print(error.text)
            case .success(let hasUpdated):
                print("Status Updated: \(hasUpdated)")
            }
        }
    }
    
    func removeFriend(friendUID: String, userUID: String, onCompletion: @escaping (_ promise: Promise<Bool>) -> Void) {
        guard let uid = PreferenceManager.shared.userId else { return }
        let query = "friends/\(uid)/"
        RealtimeDatabaseManager.shared.read(path: query) { (promise: Promise<NSDictionary>) in
            switch promise {
            case .failure(let error):
                onCompletion(Promise.failure(error))
            case .success(let dict):
                if let data = dict as? [String: Any] {
                    for (key, value) in data {
                        if let dict = value as? NSDictionary, let friendId = dict["friendId"] as? String, friendId == friendUID {
                            print("key: \(key)")
                            RealtimeDatabaseManager.shared.delete(child: "friends/\(friendUID)/\(key)") { (promise: Promise<Bool>) in
                                switch promise {
                                case .failure(let error):
                                    onCompletion(Promise.failure(error))
                                case .success(_):
                                    RealtimeDatabaseManager.shared.delete(child: "friends/\(userUID)/\(key)") { (promise: Promise<Bool>) in
                                        switch promise {
                                        case .failure(let error):
                                            onCompletion(Promise.failure(error))
                                        case .success(let isDeleted):
                                            onCompletion(Promise.success(isDeleted))
                                        }
                                    }
                                    
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func updateProfile(user: UserModel, onCompletion: @escaping(_ promise: Promise<Bool>) -> Void) {
        guard let uid = user.uid else { return }
        let query = "users/\(uid)"
        let payload = ["name": user.name, "email": user.email, "mobile": user.mobile]
        RealtimeDatabaseManager.shared.update(child: query, data: payload) { (promise: Promise<Bool>) in
            onCompletion(promise)
        }
    }
    
}
