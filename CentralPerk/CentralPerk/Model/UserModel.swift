//
//  UserModel.swift
//  CentralPerk
//
//  Created by Abhishek Kumar Ravi on 20/06/20.
//  Copyright © 2020 Abhishek Kumar Ravi. All rights reserved.
//

import Foundation

class UserModel {
    
    var name: String?
    var email: String?
    var mobile: String?
    var uid: String?
    var lastSeen: String?
    var status: UserStatus
    var profilePictureURL: String?
    var friends: [String]?
    var chatId: String?
    var isPushNotification: Bool?
    var isPublicRoom: Bool?
    var isLastSeen: Bool?
    var unreadMessageCount: Int?
    
    init(name: String?, email: String, mobile: String?, uid: String, lastSeen: String?, status: String?, profilePictureURL: String?, friends: [String], isPushNotification: Bool? = true, isPublicRoom: Bool? = true, isLastSeen: Bool? = true, unreadMessageCount: Int? = 0) {
        self.email = email
        self.name = name
        self.mobile = mobile
        self.uid = uid
        self.profilePictureURL = profilePictureURL
        self.friends = friends
        self.lastSeen = lastSeen
        self.status = UserStatus(rawValue: status ?? "") ?? .offline
        self.isPushNotification = isPushNotification
        self.isPublicRoom = isPublicRoom
        self.isLastSeen = isLastSeen
        self.unreadMessageCount = unreadMessageCount
    }
    
    convenience init?(dict: NSDictionary) {
        guard let email = dict["email"] as? String else { return nil }
        guard let uid = dict["uid"] as? String else { return nil }
        
        let name = dict["name"] as? String
        let status = dict["status"] as? String
        let lastSeen = dict["lastSeen"] as? String
        let mobile = dict["mobile"] as? String
        let profilePictureURL = dict["profilePictureURL"] as? String
        let friends = dict["friends"] as? [String]
        let isPushNotification = dict["isPushNotification"] as? Bool
        let isPublicRoom = dict["isPublicRoom"] as? Bool
        let isShowLastSeen = dict["isShowLastSeen"] as? Bool
        let unreadMessageCount = dict["unreadMessageCount"] as? Int
        
        self.init(name: name, email: email, mobile: mobile, uid: uid, lastSeen: lastSeen, status: status, profilePictureURL: profilePictureURL, friends: friends ?? [], isPushNotification: isPushNotification, isPublicRoom: isPublicRoom, isLastSeen: isShowLastSeen, unreadMessageCount: unreadMessageCount)
    }
    
    func setChatId(id: String) {
        self.chatId = id
    }
}
